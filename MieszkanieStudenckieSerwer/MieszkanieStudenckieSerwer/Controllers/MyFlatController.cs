﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using MieszkanieStudenckieSerwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Kontroler Moje mieszkanie
    /// GET - dane o moim mieszkaniu
    /// </summary>
    [IdentityBasicAuthentication]
    [Authorize]
    public class MyFlatController : ApiController
    {
        /// <summary>
        /// GET: api/MyFlat
        /// 
        /// Pobieranie informacji o mieszkaniu użytkownika
        /// </summary>
        /// <returns>Mieszkanie w JSON</returns>
        public HttpResponseMessage Get()
        {
            // Pobieranie ID mieszkania do którego należy użytkownik
            int? id = AuthorizationHelper.getUserFlatId(Request.Headers);
            if(id==null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, Responses.ACCESS_DENIED);

            // Pobieranie pełnych danych mieszkania do którego należy użytkownik i wysyłanie ich
            using (var db = new DbMieszkanieStudenckie())
            {
                Mieszkania flat = db.Mieszkanias.First(f => f.Id_mieszkania == id);
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    flat.Id_mieszkania,
                    flat.Nazwa,
                    flat.Opis,
                    flat.Ilosc_mieszkancow
                });
            }//using
        }//Get()

        /// <summary>
        /// PUT: api/MyFlat
        /// 
        /// Aktualizacja danych mieszkania
        /// </summary>
        /// <param name="flatModel">JSON z danymi mieszkania</param>
        /// <returns>Zaktualizowany obiekt mieszkania</returns>
        public HttpResponseMessage Put([FromBody]FlatModel flatModel)
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                if (AuthorizationHelper.isUserAuthorized(Request.Headers, flatModel.id))
                {
                    // aktualizacja krotki
                    var updatedFlat = new Mieszkania { Id_mieszkania = flatModel.id, Nazwa = flatModel.name, Opis = flatModel.description, haslo = flatModel.password };
                    db.Mieszkanias.Attach(updatedFlat);
                    var entry = db.Entry(updatedFlat);
                    entry.Property(e => e.Nazwa).IsModified = true;
                    entry.Property(e => e.Opis).IsModified = true;
                    entry.Property(e => e.haslo).IsModified = true;
                    db.SaveChanges();
                }//if
                else
                {
                    // odmowa dostępu
                    return Request.CreateResponse(HttpStatusCode.Forbidden, Responses.ACCESS_DENIED);
                }//else
            }//using

            // w tym samym bloku using, w którym aktualizowałem krotkę, 
            // nie mogłem pobrać prawidłowej wartości pola Ilosc_mieszkancow
            using (var db = new DbMieszkanieStudenckie())
            {
                Mieszkania flat = db.Mieszkanias.First(f => f.Id_mieszkania == flatModel.id);
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    flat.Id_mieszkania,
                    flat.Nazwa,
                    flat.Opis,
                    flat.Ilosc_mieszkancow
                });
            }//using            
        }//Put()

        /// <summary>
        /// GET: api/MyFlat/Members
        /// 
        /// Pobieranie listy współlokatorów
        /// </summary>
        /// <returns>Lista współlokatorów</returns>
        [Route("api/MyFlat/Members")]
        [HttpGet]
        public HttpResponseMessage Members()
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            int flatId = (int)AuthorizationHelper.getUserFlatId(userId);
            try
            {
                    List<dynamic> members = calculateMoney(userId, flatId);
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Uzytkownicy = members
                    });
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Members()

        /// <summary>
        /// Obliczanie kto jest ile winien użytkownikowi lub komu zalega użytkownik z opłatą
        /// </summary>
        /// <param name="userId">ID użytkownika</param>
        /// <param name="flatId">ID mieszkania</param>
        /// <returns>Lista użytkowników (nazwa i dług)</returns>
        private List<dynamic> calculateMoney(int userId, int flatId)
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                // współlokatorzy
                var users = (from user in db.Uzytkownicies
                             where user.Id_mieszkania == flatId && user.Id_uzytkownika != userId
                             select user).ToList();

                // kwota wpłacona przez użytkownika
                var data = db.Budzets.Where(d => d.Id_uzytkownika == userId);
                double? mySum = data.Sum(d => d.Cena);
                if (mySum == null)
                    mySum = 0;

                // liczba użytkowników w mieszkaniu
                double usersCount = users.Count + 1.0; // dodaję użytkownika wysyłającego zapytanie

                List<dynamic> members = new List<dynamic>();
                foreach (var user in users)
                {
                    // kwota wprowadzona przez danego współlokatora
                    var budget = db.Budzets.Where(d => d.Id_uzytkownika == user.Id_uzytkownika);
                    double? sum = budget.Sum(d => d.Cena);
                    if (sum == null)
                        sum = 0;

                    members.Add(new
                    {
                        user.Login,
                        dlug = (mySum - sum) / usersCount
                    });
                }//foreach

                return members;
            }//using
        }//calculateMoney()
    }//MyFlatController
}
