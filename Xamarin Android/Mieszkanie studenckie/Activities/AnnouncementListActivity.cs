using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "AnouncementListActivity")]
    public class AnnouncementListActivity : ListActivity
    {
        List<Model.AnnouncementModel> announcementsList;
        List<string> announcementsTopics;
        JsonValue usersJson;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            announcementsList = new List<Model.AnnouncementModel>();
            announcementsTopics = new List<string>();
            getListViewData();

            // Create your application here
        }
        private async void getListViewData()
        {
            try
            {
                var progressDialog = ProgressDialog.Show(this, "Prosz� czeka�", "Trwa wczytywanie...", true);
                JsonValue json = await ConnectionClass.getDataAsJson("api/publication");
                ParseToModel(json);
                progressDialog.Hide();
                if (announcementsTopics.Count != 0)
                    ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, announcementsTopics.ToArray());
                else
                    Toast.MakeText(this, "Brak og�osze�", ToastLength.Long).Show();
            }
            catch
            {
                Toast.MakeText(this, "Brak og�osze� do wy�wietlenia", ToastLength.Long).Show();
                base.OnBackPressed();
            }
        }
        private void ParseToModel(JsonValue json)
        {
            usersJson = json["Publications"];

            for (int i = 0; i < usersJson.Count; i++)
            {
                
                JsonValue item = usersJson[i];
                Model.AnnouncementModel announcement = new Model.AnnouncementModel();   
                announcement.publicationId = item["publicationId"];
                announcement.flatId = item["flatId"];
                announcement.userName = item["userName"];
                announcement.beginDate = DateTime.Parse(item["beginDate"]);
                // announcement.beginDate = item["beginDate"];                
                // announcement.endDate = DateTime.Parse(item["endDate"]);
                //announcement.endDate = DateTime.Parse(tempData, null, System.Globalization.DateTimeStyles.RoundtripKind);
                //announcement.endDate = item["endDate"];
                announcement.title = item["title"];
                announcement.description = item["description"];
                announcementsTopics.Add(announcement.title);
                announcementsList.Add(announcement);
                
            }

        }
        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var intent = new Intent(this, typeof(AnnouncementDetailsActivity));
            string[] userData = new string[] { announcementsList[position].userName.ToString(),announcementsList[position].beginDate.ToString("dd.MM.yyyy"),
                announcementsList[position].description.ToString(), announcementsList[position].title.ToString() };
            // intent.PutExtra("userJson", usersJson[position].ToString());
            intent.PutExtra("JsonData", userData);
            StartActivity(intent);
        }

    }
}