namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Kto_Glosowal
    {
        [Key]
        public int Id_glosu { get; set; }

        public int? Id_glosowania { get; set; }

        public int? Id_uzytkownika { get; set; }

        public bool? Glos_za { get; set; }

        public virtual Uzytkownicy Uzytkownicy { get; set; }
        public virtual Glosowanie Glosowanie { get; set; }
    }
}
