using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Mieszkanie_studenckie.Connection;
using Android.Graphics;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "BillDetailsActivity")]
    public class BillDetailsActivity : Activity
    {
        string photoString;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BillDetailsLayout);

            TextView billNameText = FindViewById<TextView>(Resource.Id.billDetailsBillNameText);
            TextView userNameText = FindViewById<TextView>(Resource.Id.billDetailsUserNameText);
            TextView priceText = FindViewById<TextView>(Resource.Id.billDetailsPriceText);
            ImageView billImage = FindViewById<ImageView>(Resource.Id.billDetailsImage);


            string[] RoomDetails = Intent.GetStringArrayExtra("JsonData");
            userNameText.Text = RoomDetails[2];
            billNameText.Text = RoomDetails[3];
            priceText.Text = RoomDetails[4];
            photoString = RoomDetails[5];

            try
            {
                byte[] imageData = Convert.FromBase64String(photoString);
                Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);

                if (bitmap != null)
                {
                    billImage.SetImageBitmap(bitmap);
                    billImage.Visibility = ViewStates.Visible;
                    bitmap = null;
                }
                else
                {
                    Toast.MakeText(this, "Brak zdj�cia", ToastLength.Long).Show();
                }

            }
            
            catch
            {
                Toast.MakeText(this, "B��d wczytywania zdj�cia", ToastLength.Long).Show();
            }


            //GetImageFromServer(billImage);

            

            // Create your application here
        }

       // private async void GetImageFromServer(ImageView imageView)
       // {
       //     var progressDialog = ProgressDialog.Show(this, "Prosz� czeka�", "Trwa wczytywanie...", true);
       //     string imgString = await ConnectionClass.getDataAsString("api/imagetest", true);
       //     progressDialog.Hide();
       //     byte[] imageData = Convert.FromBase64String(imgString);
       //
       //
       //     //Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
       //     // int height = Resources.DisplayMetrics.HeightPixels;
       //     // int width = imageView.Height;
       //     // bitmap = _file.Path.LoadAndResizeBitmap(width, height);
       //
       //    // int height = Resources.DisplayMetrics.HeightPixels;
       //     // int width = imageView.Height;
       //     Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
       //    // Bitmap bitmap = LoadAndResizeBitmap(imageData,width, height);
       //
       //     if (bitmap != null)
       //     {
       //         imageView.SetImageBitmap(bitmap);
       //         imageView.Visibility = ViewStates.Visible;
       //         bitmap = null;
       //     }
       // }
       // public  Bitmap LoadAndResizeBitmap(byte[] byteArray, int width, int height)
       // {
       //     // First we get the the dimensions of the file on disk
       //     BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
       //     BitmapFactory.DecodeByteArray(byteArray, 0, byteArray.Length, options);
       //
       //     // Next we calculate the ratio that we need to resize the image by
       //     // in order to fit the requested dimensions.
       //     int outHeight = options.OutHeight;
       //     int outWidth = options.OutWidth;
       //     int inSampleSize = 1;
       //
       //     if (outHeight > height || outWidth > width)
       //     {
       //         inSampleSize = outWidth > outHeight
       //                            ? outHeight / height
       //                            : outWidth / width;
       //     }
       //
       //     // Now we will load the image and have BitmapFactory resize it for us.
       //     options.InSampleSize = inSampleSize;
       //     options.InJustDecodeBounds = false;
       //     Bitmap resizedBitmap =  BitmapFactory.DecodeByteArray(byteArray, 0, byteArray.Length, options);
       //     return resizedBitmap;
       // }

    }
}