﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MieszkanieStudenckieSerwer.Models
{
    public class TimesheetModel
    {
        public int id;
        public int id_mieszkania;
        public int id_uzytkownika;
        public DateTime od_kiedy;
        public DateTime do_kiedy;
        public string tresc;
        
    }
}