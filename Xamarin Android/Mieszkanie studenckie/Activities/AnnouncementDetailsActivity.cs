using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "AnnouncementDetailsActivity")]
    public class AnnouncementDetailsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AnnouncementDetailsLayout);

            TextView userNameText = FindViewById<TextView>(Resource.Id.announcementDetailsUserNameText);
            TextView startDateText = FindViewById<TextView>(Resource.Id.announcementDetailsStartDateText);
            TextView titleText = FindViewById<TextView>(Resource.Id.announcementDetailsTitleText);
            TextView descriptionText = FindViewById<TextView>(Resource.Id.announcementDetailsDescriptionText);

            string[] RoomDetails = Intent.GetStringArrayExtra("JsonData");

            userNameText.Text = RoomDetails[0];
            startDateText.Text = RoomDetails[1];
            descriptionText.Text = RoomDetails[2];
            titleText.Text = RoomDetails[3];


            // Create your application here
        }
    }
}