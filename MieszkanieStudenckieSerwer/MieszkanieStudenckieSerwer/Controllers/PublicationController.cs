﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MieszkanieStudenckieSerwer.Models;
using System;
using System.Threading.Tasks;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Ogłoszenia
    /// 
    /// api/publication
    /// </summary>
    [IdentityBasicAuthentication]
    [Authorize]
    public class PublicationController : ApiController
    {
        /// <summary>
        /// GET: api/publication
        /// 
        /// Lista ogłoszeń z danego mieszkania.
        /// </summary>
        /// <returns>Lista ogłoszeń</returns>
        public HttpResponseMessage Get()
        {
            try
            {
                using (var db = new DbMieszkanieStudenckie())
                {
                    int? id = AuthorizationHelper.getUserFlatId(AuthorizationHelper.getUserId(Request.Headers));
                    var query = (from publication in db.Ogloszenias
                                 join users in db.Uzytkownicies on publication.Id_uzytkownika equals users.Id_uzytkownika
                                 where id == publication.Id_mieszkania
                                 select new
                                 {
                                     publicationId = publication.Id_ogloszenia,
                                     flatId = publication.Id_mieszkania,
                                     userName = users.Login,
                                     title = publication.Tytul,
                                     description = publication.Tresc,
                                     beginDate = publication.Od_kiedy
                                 }).ToList();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Publications = query
                    });
                }//using
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Get()

        /// <summary>
        /// POST: api/publication
        /// 
        /// Dodawanie ogłoszeń
        /// </summary>
        /// <param name="publication">Ogłoszenie do dodania</param>
        /// <returns>ID dodanego ogłoszenia lub komunikat o błędzie</returns>
        public HttpResponseMessage Post([FromBody]PublicationModel publication)
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            int? flatId = AuthorizationHelper.getUserFlatId(userId);

            if (flatId == null)
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do żadnego mieszkania");

            try
            {
                using (var db = new DbMieszkanieStudenckie())
                {
                    var publications = db.Set<Ogloszenia>();
                    Ogloszenia publicationToAdd = new Ogloszenia
                    {
                        Id_mieszkania = flatId,
                        Id_uzytkownika = userId,
                        Od_kiedy = DateTime.Now,
                        Tresc = publication.description,
                        Tytul = publication.title
                    };

                    publications.Add(publicationToAdd);
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, publicationToAdd.Id_ogloszenia);
                }//using               
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Post()

        /// <summary>
        /// PUT: api/publication
        /// 
        /// Aktualizacja istniejącego ogłoszenia.
        /// </summary>
        /// <param name="publication">Uaktualnione ogłoszenie</param>
        /// <returns>Success lub błąd</returns>
        public async Task<HttpResponseMessage> Put([FromBody]PublicationModel publication)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                int? flatId = AuthorizationHelper.getUserFlatId(userId);

                if (flatId == null)
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do żadnego mieszkania");
                try
                {
                    using (var db = new DbMieszkanieStudenckie())
                    {
                        // pobranie ogłoszenia
                        var updatedPub = db.Ogloszenias.First(p => p.Id_ogloszenia == publication.publicationId &&
                            p.Id_uzytkownika == userId && p.Id_mieszkania == flatId);

                        // aktualizacja
                        updatedPub.Tresc = publication.description;
                        updatedPub.Tytul = publication.title;

                        // zapisanie zmian w bazie
                        db.Ogloszenias.Attach(updatedPub);
                        var entry = db.Entry(updatedPub);
                        entry.Property(e => e.Tresc).IsModified = true;
                        entry.Property(e => e.Tytul).IsModified = true;
                        db.SaveChanges();
                    }//using
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Put()

        /// <summary>
        /// DELETE: api/publication
        /// 
        /// Usuwanie ogłoszenia.
        /// </summary>
        /// <param name="publication">Ogłoszenie do usunięcia (wystarczy id)</param>
        /// <returns>Success lub błąd</returns>
        public async Task<HttpResponseMessage> Delete([FromBody]PublicationModel publication)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                int? flatId = AuthorizationHelper.getUserFlatId(userId);

                if (flatId == null)
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do żadnego mieszkania");
                try
                {
                    using (var db = new DbMieszkanieStudenckie())
                    {
                        // pobranie ogłoszenia
                        var pubToDel = db.Ogloszenias.First(p => p.Id_ogloszenia == publication.publicationId &&
                            p.Id_uzytkownika == userId && p.Id_mieszkania == flatId);

                        // usunięcie ogłoszenia
                        db.Ogloszenias.Remove(pubToDel);
                        db.SaveChanges();
                    }//using
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Delete()
    }
}

