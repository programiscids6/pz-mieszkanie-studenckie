namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ogloszenia")]
    public partial class Ogloszenia
    {
        [Key]
        public int Id_ogloszenia { get; set; }

        public int? Id_mieszkania { get; set; }

        public int? Id_uzytkownika { get; set; }

        [StringLength(100)]
        public string Tytul { get; set; }

        [StringLength(250)]
        public string Tresc { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Od_kiedy { get; set; }

        public virtual Mieszkania Mieszkania { get; set; }

        public virtual Uzytkownicy Uzytkownicy { get; set; }
    }
}
