namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Budzet")]
    public partial class Budzet
    {
        [Key]
        public int Id_budzetu { get; set; }

        public int? Id_mieszkania { get; set; }

        public int? Id_uzytkownika { get; set; }

        [StringLength(30)]
        public string Nazwa { get; set; }

        public double? Cena { get; set; }

        public string Zdjecie { get; set; }

        public virtual Mieszkania Mieszkania { get; set; }

        public virtual Uzytkownicy Uzytkownicy { get; set; }
    }
}
