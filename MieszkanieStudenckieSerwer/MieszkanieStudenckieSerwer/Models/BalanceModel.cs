﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MieszkanieStudenckieSerwer.Models
{
    public class BalanceModel
    {
        public double myCashContribution { get; set; }
        public List<UsersBalance> usersBalance { get; set; }
        public class UsersBalance
        {
            public string username { get; set; }
            public double balance { get; set; }
        }//UsersBalance

        public BalanceModel(double cashContribution)
        {
            myCashContribution = cashContribution;
            usersBalance = new List<UsersBalance>();
        }//BalanceModel()

        public BalanceModel()
        {
            myCashContribution = 0;
            usersBalance = new List<UsersBalance>();
        }//BalanceModel()
    }//BalanceModel
}