using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "AddAnnouncementActivity")]
    public class AddAnnouncementActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddAnnouncementLayout);

            EditText TitleText = FindViewById<EditText>(Resource.Id.addAnnouncementTitleText);
            EditText DescriptionText = FindViewById<EditText>(Resource.Id.addAnnouncementDescriptionText);
            Button addRoomButton = FindViewById<Button>(Resource.Id.addAnnouncementButton);

            addRoomButton.Click += async (sender, e) =>
            {
                if (String.IsNullOrEmpty(TitleText.Text))
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Temat og�oszenia nie mo�e by� pusta.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { Finish(); });
                        builder.Show();
                    }
                  );
                }
               else if (String.IsNullOrEmpty(DescriptionText.Text))
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Tre�� og�oszenia nie mo�e by� pusta.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { Finish(); });
                        builder.Show();
                    }
                  );
                }
                else
                {
                    string roomJSON = @" {""title"":""" + TitleText.Text + @""", ""description"":""" + DescriptionText.Text + @""" } ";
                    //roomName = @" "" } " +roomName ; 
                    string response =  await SendData("api/publication", roomJSON);
                    Toast.MakeText(this, "Dodano og�oszenie", ToastLength.Long).Show();
                    base.OnBackPressed();
                }
            };
            // Create your application here
        }

        private async Task<string> SendData(string url, string data)
        {
            return await Connection.ConnectionClass.sendData(url, data);
        }
    }
}