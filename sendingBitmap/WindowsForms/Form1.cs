﻿using Mieszkanie_studenckie.Connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // wysyłanie obrazu
        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
                if (open.ShowDialog() == DialogResult.OK)
                {
                    // otworzenie i wysłanie obrazu
                    Bitmap bit = new Bitmap(open.FileName);
                    
                    Console.Out.WriteLine("Wczytano");

                    byte[] imageBytes = ImageToByte(bit);
                    // konwersja byte[] do base64string
                    string base64String = Convert.ToBase64String(imageBytes);
                    string desc = "mój obraz";

                    string data = "{ \"image\":\"" + base64String + "\", \"description\":\"" + desc + "\" }";

                    await ConnectionClass.sendData("api/ImageTest", data);

                    Console.Out.WriteLine("Wysyłanie"); ;
                }
            }
            catch (Exception)
            {
                throw new ApplicationException("Failed loading image");
            }
            
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        // pobieranie obrazu
        private async void button2_Click(object sender, EventArgs e)
        {
            string imgString = await ConnectionClass.getDataAsString("api/imagetest", true);

            byte[] imageData = Convert.FromBase64String(imgString);

            Bitmap bmp;
            using (var ms = new MemoryStream(imageData))
            {
                bmp = new Bitmap(ms);
                SaveFileDialog dialog = new SaveFileDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    bmp.Save(dialog.FileName, ImageFormat.Bmp);
                    Console.Out.WriteLine("Zapisano");
                }
            }

            
        }
    }
}
