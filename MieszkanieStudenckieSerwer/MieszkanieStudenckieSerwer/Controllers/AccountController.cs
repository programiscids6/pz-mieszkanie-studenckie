﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using MieszkanieStudenckieSerwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Kontroler konta. Umożliwia zarządzanie kontem (logowanie, rejestrację i modyfikację danych)
    /// </summary>
    public class AccountController : ApiController
    {
        /// <summary>
        /// Logowanie
        /// </summary>
        /// <returns>Łańcuch znaków: 
        ///  - Success w przypadku udanego logowania
        ///  - informację o błędzie gdy dane są niepoprawne</returns>
        [IdentityBasicAuthentication]
        [Authorize]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
        }//Get()

        /// <summary>
        /// Rejestracja
        /// </summary>
        /// <param name="user">Obiekt klasy pomocniczej UserModel zawierający dane o użytkowniku</param>
        /// <returns></returns>
        [Route("api/account/register")]
        [HttpPost]
        public HttpResponseMessage Register(UserModel user)
        {
            // insert
            using (var db = new DbMieszkanieStudenckie())
            {
                string existStrUser = null;
                var users = db.Set<Uzytkownicy>();
                try
                {
                    string checkUniq = (from existUser in users
                                        where existUser.Login == user.username
                                        select existUser).First().ToString();
                    existStrUser = checkUniq;
                } catch(Exception)
                {
                    existStrUser = null;
                }//catch

                if (existStrUser == null)
                {
                    users.Add(new Uzytkownicy { Id_mieszkania = null, Login = user.username, Haslo = user.password });

                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//if
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.LOGIN_IN_USE);
                }//else
            }//using
        }//Register()

        /// <summary>
        /// Dodawanie użytkownika do mieszkania
        /// </summary>
        /// <param name="flat">Dane mieszkania do którego należy dodać użytkownika w formacie JSON:
        /// {
        ///     "id":id,
        ///     "password":"haslo"
        /// } 
        /// </param>
        /// <returns>Success jeśli się powiodło lub komunikat o błędzie.</returns>
        [IdentityBasicAuthentication]
        [Authorize]
        [HttpPut]
        [Route("api/account/addToFlat")]
        public HttpResponseMessage addToFlat(FlatModel flat)
        {

            if (!checkPassword(flat.password, flat.id))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);

            try
            {
                string username = AuthorizationHelper.getUserName(Request.Headers);
                using (var db = new DbMieszkanieStudenckie())
                {
                    var updatedUser = db.Uzytkownicies.First(u => u.Login == username);
                    //var updatedUser = new Uzytkownicy { Id_uzytkownika = user.Id_uzytkownika, Id_mieszkania = userModel.flatId, Login = userModel.username };
                    updatedUser.Id_mieszkania = flat.id;
                    db.Uzytkownicies.Attach(updatedUser);
                    var entry = db.Entry(updatedUser);
                    entry.Property(e => e.Id_mieszkania).IsModified = true;
                    db.SaveChanges();
                }//using
            }//try
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.ToString());
            }//catch

            return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
        }//Put()

        /// <summary>
        /// Sprawdzanie poprawności hasła
        /// </summary>
        /// <param name="password">Hasło do mieszkania</param>
        /// <param name="flatId">ID mieszkania</param>
        /// <returns>true - hasło poprawne, false - hasło niepoprawne</returns>
        private static bool checkPassword(string password, int flatId)
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                Mieszkania flat;
                try
                {
                    flat = db.Mieszkanias.First(m => m.haslo == password);
                } catch(InvalidOperationException)
                {
                    return false;
                }
                if (flat != null)
                    return true;
            }//using

            return false;
        }//checkPassword()

    }//AccountController
}
