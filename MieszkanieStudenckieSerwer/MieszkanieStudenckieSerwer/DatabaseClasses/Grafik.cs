namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Grafik")]
    public partial class Grafik
    {
        [Key]
        public int Id_grafiku { get; set; }

        public int? Id_mieszkania { get; set; }

        public int? Id_uzytkownika { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Od_kiedy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Do_kiedy { get; set; }

        [StringLength(100)]
        public string Tresc { get; set; }

        public virtual Mieszkania Mieszkania { get; set; }

        public virtual Uzytkownicy Uzytkownicy { get; set; }
    }
}
