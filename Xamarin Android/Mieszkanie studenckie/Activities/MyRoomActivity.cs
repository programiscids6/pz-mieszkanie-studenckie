using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "MyRoomActivity")]
    public class MyRoomActivity : Activity
    {
        public string roomName;
        public string description;
        public int roomId;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MyRoomLayout);

            Button addBillButton = FindViewById<Button>(Resource.Id.myRoomAddBillButton);
            Button billPerPersonButton = FindViewById<Button>(Resource.Id.myRoomBillPerPersonButton);
            Button billsButton = FindViewById<Button>(Resource.Id.myRoomBillsButton);            
            Button announcementsButton = FindViewById<Button>(Resource.Id.myRoomAnnouncementsButton);
            Button addAnnouncementButton = FindViewById<Button>(Resource.Id.myRoomAddAnnouncementButton);
            Button settingsButton = FindViewById<Button>(Resource.Id.myRoomSettingsButton);

           // var progressDialog = ProgressDialog.Show(this, "Prosz� czeka�", "Trwa wysy�anie...", true);           
            
            GetData();
           // progressDialog.Hide();


            addBillButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.AddBillActivity));
                string[] userData = new string[] { roomId.ToString() };
                // intent.PutExtra("userJson", usersJson[position].ToString());
                intent.PutExtra("JsonData", userData);
                StartActivity(intent);
            };

            billPerPersonButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.MembersListActivity));
                StartActivity(intent);
            };

            billsButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.BillsListActivity));
                StartActivity(intent);
            };

            announcementsButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.AnnouncementListActivity));
                StartActivity(intent);
            };

            addAnnouncementButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.AddAnnouncementActivity));
                StartActivity(intent);
            };

            settingsButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.MyRoomDetailsActivity));
                string[] userData = new string[] { roomName, description, roomId.ToString() };
                // intent.PutExtra("userJson", usersJson[position].ToString());
                intent.PutExtra("JsonData", userData);
                StartActivity(intent);
            };

            // Create your application here
        }
        private async void GetData()
        {
            try
            {
                var progressDialog = ProgressDialog.Show(this, "Prosz� czeka�", "Pobieranie danych mieszkania...", true);
                JsonValue json = await ConnectionClass.getDataAsJson("api/myflat");
                roomName = json["Nazwa"];
                description = json["Opis"];
                roomId = json["Id_mieszkania"];
                progressDialog.Hide();
            }
            catch
            {
                Toast.MakeText(this, "Nie zosta�e� przypisany do �adnego mieszkania", ToastLength.Long).Show();
                base.OnBackPressed();
            }
        }
    }
}