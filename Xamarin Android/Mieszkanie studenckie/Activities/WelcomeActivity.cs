using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie
{
    [Activity(Label = "WelcomeActivity")]
    public class WelcomeActivity : Activity
    {
      
        protected override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                base.OnCreate(savedInstanceState);
                // Create your application here
                //usersList = new List<Model.UserModel>();
                SetContentView(Resource.Layout.WelcomeLayout);

                string login = Intent.GetStringExtra("Login") ?? "B��dny login";

                TextView LoginTextView = FindViewById<TextView>(Resource.Id.LoginTextView);
                Button findRoomsButton = FindViewById<Button>(Resource.Id.findRoomButton);
                Button addRoomButton = FindViewById<Button>(Resource.Id.addRoomsButton);
                Button myRoomButton = FindViewById<Button>(Resource.Id.myRoomButton);
                Button logoutButton = FindViewById<Button>(Resource.Id.logoutButton);

                LoginTextView.Text = "Witaj " + login + "!";

                findRoomsButton.Click += (sender, e) =>
                {
                    var intent = new Intent(this, typeof(Activities.RoomsListActivity));
                    StartActivity(intent);
                };

                addRoomButton.Click += (sender, e) =>
                {
                    var intent = new Intent(this, typeof(Activities.AddRoomActivity));
                    StartActivity(intent);
                };
                myRoomButton.Click += (sender, e) =>
                {
                    var intent = new Intent(this, typeof(Activities.MyRoomActivity));
                    StartActivity(intent);
                };

                logoutButton.Click += (sender, e) =>
                {
                    var intent = new Intent(this, typeof(MainActivity));
                    intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);                    
                    StartActivity(intent);
                };

                
            }
            catch
            {
                Toast.MakeText(this, "Wyst�pi� b��d. Spr�buj ponownie", ToastLength.Long).Show();
                base.OnBackPressed();
            }

        }
    }
}