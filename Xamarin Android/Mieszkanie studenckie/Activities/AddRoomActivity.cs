using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "AddRoomActivity")]
    public class AddRoomActivity : Activity
    {
       // string roomName;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddRoomLayout);

            EditText roomNameText = FindViewById<EditText>(Resource.Id.addRoomNameText);
            EditText roomPasswordText = FindViewById<EditText>(Resource.Id.addRoomPasswordText);
            EditText roomDescriptionText = FindViewById<EditText>(Resource.Id.addRoomDescriptionText);
            Button addRoomButton = FindViewById<Button>(Resource.Id.addRoomButton);

          //  roomName = " {\"name\":\" "+ addRoomText.Text+ " \"} ";
          // SendData("api/flats",roomName);

            addRoomButton.Click += (sender, e) =>
            {
                if (String.IsNullOrEmpty(roomNameText.Text) )
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Nazwa mieszkania nie mo�e by� pusta.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { Finish(); });
                        builder.Show();
                    }
                  );
                }
                else if (String.IsNullOrEmpty(roomPasswordText.Text))
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Has�o nie mo�e by� puste.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { Finish(); });
                        builder.Show();
                    }
                  );
                }
                else if (String.IsNullOrEmpty(roomDescriptionText.Text))
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Opis nie mo�e by� pusty.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { Finish(); });
                        builder.Show();
                    }
                  );
                }
                else
                {
                    string roomJSON = @" {""name"":""" + roomNameText.Text + @""", ""description"":""" + roomDescriptionText.Text + @""", ""password"":""" + roomPasswordText.Text+ @""" } ";
                    //roomName = @" "" } " +roomName ; 
                    SendData("api/flats", roomJSON);
                    Toast.MakeText(this, "Dodano pok�j", ToastLength.Long).Show();
                    base.OnBackPressed();
                }  
            };

            // Create your application here
        }

        private async void SendData(string url, string data)
        {
             await Connection.ConnectionClass.sendData(url, data);
        }

    }
}