namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Glosowanie")]
    public partial class Glosowanie
    {
        [Key]
        public int Id_glosowania { get; set; }

        public int? Id_mieszkania { get; set; }

        [StringLength(30)]
        public string Nazwa { get; set; }

        [StringLength(250)]
        public string Opis { get; set; }

        public int? Glosy_za { get; set; }

        public int? Glosy_przeciw { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Od_kiedy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Do_kiedy { get; set; }

        public virtual Mieszkania Mieszkania { get; set; }
    }
}
