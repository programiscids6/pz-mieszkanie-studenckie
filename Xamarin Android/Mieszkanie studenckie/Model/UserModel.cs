using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Model
{
    public class UserModel
    {
        public int Id_uzytkownika { get; set; }
        public int Id_mieszkania { get; set; }
        public string Login { get; set; }
        public string Haslo { get; set; }
        public string Opis { get; set; }
    }
}