﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MieszkanieStudenckieSerwer.Models;
using System;
namespace MieszkanieStudenckieSerwer.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class WhovoteController : ApiController
    {
        public HttpResponseMessage Get()
        // Zwraca wszytkie grafiki z mieszkania aktualnie zalogowaneg uzytkownika
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                int? id = AuthorizationHelper.getUserId(Request.Headers);
                var query = (from Kto_glosowal in db.Kto_Glosowal
                             where id == Kto_glosowal.Id_uzytkownika
                             select new
                             {
                                 Kto_glosowal.Id_glosowania,
                                 Kto_glosowal.Id_uzytkownika,
                                 Kto_glosowal.Glos_za                          

                             }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Kto_glosowal = query
                });

            }



        }
    }
}

