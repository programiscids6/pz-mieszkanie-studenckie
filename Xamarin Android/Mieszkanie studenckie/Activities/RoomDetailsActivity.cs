using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "RoomDetailsActivity")]
    public class RoomDetailsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            try
            {

                base.OnCreate(savedInstanceState);
                SetContentView(Resource.Layout.RoomDetailsLayout);

                TextView RoomNameText = FindViewById<TextView>(Resource.Id.RoomNameText);
                TextView ResidentsCountText = FindViewById<TextView>(Resource.Id.ResidentsCountText);
                TextView RoomDescriptionText = FindViewById<TextView>(Resource.Id.RoomDescriptionText);
                EditText RoomPasswordText = FindViewById<EditText>(Resource.Id.RoomPasswordText);
                Button JoinToRoomButton = FindViewById<Button>(Resource.Id.JoinRoomButton);

                string[] RoomDetails = Intent.GetStringArrayExtra("JsonData");

                RoomNameText.Text = RoomDetails[0];
                ResidentsCountText.Text = RoomDetails[1];
                RoomDescriptionText.Text = RoomDetails[2];
                string roomID = RoomDetails[3];
                // Create your application here

                JoinToRoomButton.Click += async(sender, e) =>
                {
                    if (String.IsNullOrEmpty(RoomPasswordText.Text))
                    {
                        RunOnUiThread(() =>
                        {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(this);
                            builder.SetTitle("B��d");
                            builder.SetMessage("Has�o nie mo�e by� puste.");
                            builder.SetCancelable(false);
                            builder.SetPositiveButton("OK", delegate { Finish(); });
                            builder.Show();
                        }
                      );
                    }

                    else
                    {
                        string roomJSON = @" {""id"":""" + roomID + @""", ""password"":""" + RoomPasswordText.Text + @""" } ";
                        //roomName = @" "" } " +roomName ; 
                        string response = await SendData("api/account/addToFlat", roomJSON, "PUT");
                        Toast.MakeText(this, "Dodano do pokoju", ToastLength.Long).Show();
                        base.OnBackPressed();
                        base.OnBackPressed();
                    }
                };
            }
            catch
            {
                Toast.MakeText(this, "B��d �adowania pokoju", ToastLength.Long).Show();
                base.OnBackPressed();
            }

        }
        private async Task<string> SendData(string url, string data, string method)
        {
            return await Connection.ConnectionClass.sendData(url, data, method);
        }
    }
}