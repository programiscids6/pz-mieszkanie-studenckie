use [Mieszkanie studenckie]
 -- Ten trigger gdy tworzone jest nowe mieszkanie ustawia ilo�� mieszka�c�w na 1
CREATE TRIGGER nowe_mieszkanie 
 ON Mieszkania
  AFtER INSERT
  as
begin 
update Mieszkania Set Ilosc_mieszkancow=1;
end;

-- Ten trigger zwi�ksza ilo�� mieszka�c�w gdy do mieszkania zostanie dodany nowy mieszkaniec
CREATE TRIGGER nowy_mieszkaniec
 ON Uzytkownicy
  AFtER Insert,Update
  as
begin 

update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow+1
From Inserted i ,Mieszkania m
where i.Id_mieszkania != 0 and i.Id_mieszkania = m.Id_mieszkania

end;
-- Zmniejsza ilo�� mieszka�c�w po usunieciu u�ytkownika 
CREATE TRIGGER usuni�cie_mieszkanca
 ON Uzytkownicy
  AFtER delete
  as
begin 

update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow-1
From deleted d ,Mieszkania m
where d.Id_mieszkania != 0 and d.Id_mieszkania = m.Id_mieszkania

end;
-- Zmniejsza liczb� mieszka�c�w gdy kt�ry� zostanie usuni�ty
CREATE TRIGGER wywalenie_mieszkanca
 ON Uzytkownicy
  after update
  as
begin 

update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow-1
From inserted i ,Mieszkania m, Deleted d
where  d.Id_mieszkania = m.Id_mieszkania and i.Id_mieszkania is null 
end;

drop trigger  wywalenie_mieszkanca

-- Dane testowe
INSERT INTO Mieszkania
VALUES ('Mieszkanie Janasad',0);

drop Trigger nowy_mieszkaniec
INSERT INTO Uzytkownicy
VALUES (3, 'Jahgyvyvhn', 'jan@wp.pl','haslo');
INSERT INTO Mieszkania
VALUES ('Mieszkanie Jana', 2);

SELECT * FROM Uzytkownicy;
SELECT * FROM Mieszkania;
delete from Uzytkownicy

Update Uzytkownicy
Set Id_mieszkania =null