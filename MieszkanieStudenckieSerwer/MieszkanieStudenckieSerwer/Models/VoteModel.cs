﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MieszkanieStudenckieSerwer.Models
{
    public class VoteModel
    {
        public int voteId { get; set; }
        public int flatId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int voteYes { get; set; }
        public int voteNo { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public bool userVote { get; set; }// true - tak, false - nie
    }
}