using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Mieszkanie_studenckie.Connection;
using Android.Net;

namespace Mieszkanie_studenckie
{
    [Activity(Label = "RegisterActivity")]
    public class RegisterActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.registerLayout);
            Button registerButton = FindViewById<Button>(Resource.Id.regButton);            
            EditText loginText = FindViewById<EditText>(Resource.Id.usernameText);
            EditText passwordText = FindViewById<EditText>(Resource.Id.passText);

            registerButton.Click += async (sender, e) =>
            {
                try
                {
                    if (String.IsNullOrEmpty(loginText.Text) || String.IsNullOrEmpty(passwordText.Text))
                    {
                        Toast.MakeText(this, "Wpisz dane logowania", ToastLength.Long).Show();
                    }
                    else
                    {
                        ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
                        NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;

                        bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

                        if (isOnline)
                        {
                            var progressDialog = ProgressDialog.Show(this, "Prosz� czeka� ...", "Trwa rejestracja...", true);

                            // wys�anie ��dania logowania i pobranie odpowiedzi z serwera
                            string data = "{ \"username\":\"" + loginText.Text + "\", \"password\":\"" + passwordText.Text + "\" }";
                            string response = await ConnectionClass.sendData("api/account/register", data);
                            //Console.Out.WriteLine("[Rejestracja] Odpowied� serwera: " + response);
                            progressDialog.Hide();
                            if (response.Equals("\"Success\""))
                            {
                                Toast.MakeText(this, "Poprawnie dodano u�ytkownika", ToastLength.Long).Show();
                                base.OnBackPressed();
                            }//if
                            else
                            {
                                Toast.MakeText(this, response, ToastLength.Long).Show();
                            }

                        }
                        else
                        {
                            Toast.MakeText(this, "Sprawd� po��czenie internetowe", ToastLength.Long).Show();
                        }
                    }
                }
                catch
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d podczas rejestracji");
                        builder.SetMessage("Prosz� spr�bowa� ponownie.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { });
                        builder.Show();
                    }
                    );
                }

            };            

          }
    }
}