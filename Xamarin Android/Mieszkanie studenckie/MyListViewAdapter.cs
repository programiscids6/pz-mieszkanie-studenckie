using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Mieszkanie_studenckie.Model;

namespace Mieszkanie_studenckie
{
    public class MyListViewAdapter : BaseAdapter<MemberBudgetModel>
    {

        private List<MemberBudgetModel> mItems;
        private Context mContext;

        public MyListViewAdapter(Context context, List<MemberBudgetModel> items)
        {
            mItems = items;
            mContext = context;
        }
        public override int Count
        {
            get { return mItems.Count; }      
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override MemberBudgetModel this[int position]
        {
            get { return mItems[position]; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            if (row == null)
            {
                row = LayoutInflater.From(mContext).Inflate(Resource.Layout.MembersListRowLayout, null, false);
            }
            TextView nameTxt = row.FindViewById<TextView>(Resource.Id.MembersListRowNameText);
            nameTxt.Text = mItems[position].login;

            TextView dlugtxt = row.FindViewById<TextView>(Resource.Id.MembersListRowMoneyText);
            dlugtxt.Text = mItems[position].dlug.ToString();

            return row;

        }
    }
}