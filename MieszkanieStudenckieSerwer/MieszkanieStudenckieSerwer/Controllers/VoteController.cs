﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MieszkanieStudenckieSerwer.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// api/vote
    /// 
    /// Głosowania
    /// </summary>
    [IdentityBasicAuthentication]
    [Authorize]
    public class VoteController : ApiController
    {
        /// <summary>
        /// GET: api/vote
        /// 
        /// Zwracanie listy trwających głosowań
        /// </summary>
        /// <returns>Lista trwających głosowań, JSON:
        /// {
        ///	"Votes": [
		///    {
		///	    "voteId": idGłosowania<int>,
		/// 	"flatId": idMieszkania<int>,
		/// 	"name": nazwaGłosowania<string>,
		/// 	"description": opisGłosowania<string>,
		/// 	"voteYes": liczbaGłosówZa<int>,
		/// 	"voteNo": liczbaGłosówPrzeciw<int>,
		/// 	"beginDate": dataRozpoczęciaGłosowania<DateTime>,
		/// 	"endDate": dataZakończeniaGłosowania<DateTime>
        /// 	"avaliable": czyUżytkownikMożeOddaćGłos<bool>,
        /// 	"myVote": głosUżytkownika<bool?>
        ///    }
	    ///   ]
        /// }
        /// </returns>
        public HttpResponseMessage Get()
        {
            try
            {
                int? id = AuthorizationHelper.getUserFlatId(AuthorizationHelper.getUserId(Request.Headers));
                int userId = AuthorizationHelper.getUserId(Request.Headers);

                List<dynamic> votes = getVotes(id, userId);
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Votes = votes
                });
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Get()

        private List<dynamic> getVotes(int? flatId, int userId)
        {
            if (flatId == null)
                return new List<dynamic>();

            List<dynamic> votesList = new List<dynamic>();
            using (var db = new DbMieszkanieStudenckie())
            {
                var votes = db.Glosowanies.Where(v => v.Id_mieszkania == flatId);
                foreach (var vote in votes)
                {
                    bool voteAvaliable = false;
                    if (vote.Do_kiedy > DateTime.Now)
                        voteAvaliable = firstVote(vote.Id_glosowania, userId, flatId);

                    bool? usersVote = null;
                    if (!voteAvaliable && vote.Do_kiedy > DateTime.Now)
                    {
                        usersVote = db.Kto_Glosowal.First(v => v.Id_glosowania == vote.Id_glosowania && v.Id_uzytkownika == userId).Glos_za;
                    }//if

                    votesList.Add(new
                    {
                        voteId = vote.Id_glosowania,
                        flatId = vote.Id_mieszkania,
                        name = vote.Nazwa,
                        description = vote.Opis,
                        voteYes = vote.Glosy_za,
                        voteNo = vote.Glosy_przeciw,
                        beginDate = vote.Od_kiedy,
                        endDate = vote.Do_kiedy,
                        avaliable = voteAvaliable,
                        myVote = usersVote
                    });
                }//foreach
            }//using

            return votesList;
        }//getVotes()

        /// <summary>
        /// POST: api/vote
        /// 
        /// Dodawanie nowego głosowania
        /// </summary>
        /// <param name="vote">Głosowanie do dodania, JSON: 
        ///    {
        /// 	"name": nazwaGłosowania<string>,
        /// 	"description": opisGłosowania<string>,
        /// 	"endDate": dataZakończeniaGłosowania<DateTime>
        ///    }
        /// </param>
        /// <returns>ID głosowania<int></returns>
        public HttpResponseMessage Post([FromBody]VoteModel vote)
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            int? flatId = AuthorizationHelper.getUserFlatId(userId);

            if (flatId == null)
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do żadnego mieszkania");

            try
            {
                using (var db = new DbMieszkanieStudenckie())
                {
                    var votes = db.Set<Glosowanie>();
                    Glosowanie voteToAdd = new Glosowanie
                    {
                        Id_mieszkania = flatId,
                        Do_kiedy = vote.endDate,
                        Od_kiedy = DateTime.Now,
                        Opis = vote.description,
                        Glosy_za = 0,
                        Glosy_przeciw = 0,
                        Nazwa = vote.name
                    };

                    votes.Add(voteToAdd);
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, voteToAdd.Id_glosowania);
                }//using               
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Post()

        /// <summary>
        /// PUT: api/vote
        /// 
        /// Oddanie głosu
        /// </summary>
        /// <param name="vote">ID głosowania i oddany głos, JSON: 
        /// {
        ///     "voteId": idGłosowania(int),
        ///     "userVote": głosUżytkownika(bool)
        /// } 
        /// gdzie userVote==true oznacza głos na tak
        /// </param>
        /// <returns>Success w przypadku pomyślnego oddania głosu lub błąd.</returns>
        public async Task<HttpResponseMessage> Put([FromBody]VoteModel vote)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                int? flatId = AuthorizationHelper.getUserFlatId(userId);

                if (flatId == null)
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do żadnego mieszkania.");

                if (!firstVote(vote.voteId, userId, flatId))
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik już głosował.");

                try
                {
                    using (var db = new DbMieszkanieStudenckie())
                    {
                        // pobranie ogłoszenia
                        var updatedVote = db.Glosowanies.First(p => p.Id_glosowania == vote.voteId &&
                            p.Id_mieszkania == flatId);

                        if (updatedVote.Do_kiedy < DateTime.Now)
                            return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Głosowanie już się zakończyło.");

                        // aktualizacja
                        if (vote.userVote)
                        {
                            updatedVote.Glosy_za++;
                        }//if
                        else
                        {
                            updatedVote.Glosy_przeciw++;
                        }//else

                        // aktualizacja informacji o tym kto głosował
                        Kto_Glosowal whoVote = new Kto_Glosowal { Id_glosowania = updatedVote.Id_glosowania, Id_uzytkownika = userId, Glos_za = vote.userVote };
                        db.Kto_Glosowal.Add(whoVote);

                        // zapisanie zmian w bazie
                        db.Glosowanies.Attach(updatedVote);
                        var entry = db.Entry(updatedVote);
                        entry.Property(e => e.Glosy_za).IsModified = true;
                        entry.Property(e => e.Glosy_przeciw).IsModified = true;
                        db.SaveChanges();
                    }//using
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Put()

        /// <summary>
        /// Sprawdza czy jest to pierwszy głos użytkownika w danym głosowaniu.
        /// </summary>
        /// <param name="voteId">ID głosowania</param>
        /// <param name="userId">ID użytkownika</param>
        /// <param name="flatId">ID mieszkania</param>
        /// <returns>true - pierwszy głos, false - próba ponownego głosowania.</returns>
        private bool firstVote(int voteId, int userId, int? flatId)
        {
            try
            {
                using (var db = new DbMieszkanieStudenckie())
                {
                    var whoVote = db.Kto_Glosowal.First(w => w.Id_glosowania == voteId && w.Id_uzytkownika == userId);
                }//using
                return false;
            }//try
            catch (Exception ex)
            {
                string exception = ex.ToString();
                Console.Out.WriteLine(exception);
            }//catch
            return true;
        }//firstVote()
    }
}
