using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "MyRoomDetailActivity")]
    public class MyRoomDetailsActivity : Activity
    {
        public string roomName;
        public string roomDescription;
        public int roomId;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MyRoomDetailsLayout);

            TextView roomNameText = FindViewById<TextView>(Resource.Id.myRoomDetailsNameText);
            TextView roomDescriptionText = FindViewById<TextView>(Resource.Id.myRoomDetailsDescriptionText);
            Button editRoomButton = FindViewById<Button>(Resource.Id.myRoomDetailsButton);
            Button refreshButton = FindViewById<Button>(Resource.Id.myRoomDetailsRefreshButton);
            

            string[] RoomDetails = Intent.GetStringArrayExtra("JsonData");
            roomName = RoomDetails[0];
            roomDescription = RoomDetails[1];
            roomId = Int32.Parse(RoomDetails[2]);

            roomNameText.Text = roomName;
            roomDescriptionText.Text = roomDescription;


            

            editRoomButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(Activities.MyRoomEditActivity));
                string[] userData = new string[] {roomName, roomDescription, roomId.ToString()};
                // intent.PutExtra("userJson", usersJson[position].ToString());
                intent.PutExtra("JsonData", userData);
                StartActivity(intent);                
            };

            refreshButton.Click += (sender, e) =>
            {
                GetData(roomNameText, roomDescriptionText);
            };

        }
        //protected override void OnResume()
        //{
        //    TextView roomNameText = FindViewById<TextView>(Resource.Id.myRoomDetailsNameText);
        //    TextView roomDescriptionText = FindViewById<TextView>(Resource.Id.myRoomDetailsDescriptionText);
        //    Button editRoomButton = FindViewById<Button>(Resource.Id.myRoomDetailsButton);
        //    GetData(roomNameText, roomDescriptionText);
        //
        //    roomNameText.Text = roomName;
        //    roomDescriptionText.Text = description;
        //
        //}

        private async void GetData(TextView nameText, TextView descriptionText)
        {
            JsonValue json = await ConnectionClass.getDataAsJson("api/myflat");
       
            roomName = json["Nazwa"];
            roomDescription = json["Opis"];
            roomId = json["Id_mieszkania"];
       
            nameText.Text = roomName;
            descriptionText.Text = roomDescription;
        }

    }

        // Create your application here
}
   