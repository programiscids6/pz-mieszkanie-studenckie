﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MieszkanieStudenckieSerwer.Helpers
{
    /// <summary>
    /// Klasa statyczna zawierająca podstawowe odpowiedzi serwera
    /// </summary>
    public static class Responses
    {
        public static string SUCCESS = "Success";
        public static string FAIL = "Fail";
        public static string ACCESS_DENIED = "Odmowa dostępu. Nie możesz modyfikować tego zasobu.";
        public static string NOT_FOUND = "Nie znaleziono";

        public static string LOGIN_IN_USE = "Login jest zajęty";
    }//Responses
}