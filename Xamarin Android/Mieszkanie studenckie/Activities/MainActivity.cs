﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Mieszkanie_studenckie.Connection;
using Android.Net;
using System.Threading;

namespace Mieszkanie_studenckie
{
    [Activity(Label = "Mieszkanie studenckie", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        public static RestService RestService { get; private set; }

        public MainActivity()
        {
            RestService = new RestService();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button loginButton = FindViewById<Button>(Resource.Id.loginButton);
            Button registerButton = FindViewById<Button>(Resource.Id.registerButton);
            EditText loginText = FindViewById<EditText>(Resource.Id.loginText);
            EditText passwordText = FindViewById<EditText>(Resource.Id.passwordText);

            loginButton.Click += async (sender, e) =>
            {
                try
                {
                    if (String.IsNullOrEmpty(loginText.Text) || String.IsNullOrEmpty(passwordText.Text))
                    {
                        Toast.MakeText(this, "Wpisz dane logowania", ToastLength.Long).Show();
                    }
                    else
                    {
                        ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
                        NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;

                        bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

                        if (isOnline)
                        {
                            var progressDialog = ProgressDialog.Show(this, "Proszę czekać ...", "Trwa logowanie...", true);

                            //new Thread(new ThreadStart(delegate
                            //{
                            //    //LOAD METHOD TO GET ACCOUNT INFO
                            //    System.Threading.Thread.Sleep(10000);
                            //    RunOnUiThread(() => Toast.MakeText(this, "Toast within progress dialog.", ToastLength.Long).Show());
                            //    //HIDE PROGRESS DIALOG
                            //    RunOnUiThread(() => progressDialog.Hide());
                            //})).Start();

                            // wysłanie żądania logowania i pobranie odpowiedzi z serwera
                            bool loginStatus = await ConnectionClass.login(loginText.Text, passwordText.Text);
                            progressDialog.Hide();
                            Console.Out.WriteLine("Odpowiedź serwera: " + loginStatus);

                            // jeśli zalogowano to przejdź do kolejnego widoku
                            if (loginStatus)
                            {
                                var intent = new Intent(this, typeof(WelcomeActivity));
                                intent.PutExtra("Login", loginText.Text);
                                StartActivity(intent);
                            }//if
                            else
                            {
                                Toast.MakeText(this, "Zły login lub hasło", ToastLength.Long).Show();
                            }
                        }
                        else
                        {
                            Toast.MakeText(this, "Sprawdź połączenie internetowe", ToastLength.Long).Show();
                        }
                    }
                }
                catch
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("Błąd podczas logowania");
                        builder.SetMessage("Proszę spróbować ponownie.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { });
                        builder.Show();
                    }
                    );
                }
            };

            registerButton.Click +=  (sender, e) =>
            {                
                    var intent = new Intent(this, typeof(RegisterActivity));                    
                    StartActivity(intent);                
            };



        }
    }
}

