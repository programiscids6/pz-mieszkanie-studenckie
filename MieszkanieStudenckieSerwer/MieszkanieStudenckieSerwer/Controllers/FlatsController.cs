﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MieszkanieStudenckieSerwer.Models;
using System;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Kontroler mieszkań
    /// GET - lista mieszkań
    /// POST - dodawanie nowego mieszkania
    /// PUT - dodawanie zmian do istniejącego mieszkania
    /// 
    /// TODO: usunąć DELETE
    /// </summary>
    [IdentityBasicAuthentication]
    [Authorize]
    public class FlatsController : ApiController
    {
        /// <summary>
        /// Wysyłanie listy mieszkań
        /// </summary>
        /// 
        /// <returns>Lista mieszkań (JSON)</returns>
        public HttpResponseMessage Get()
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                var query = (from mieszkanie in db.Mieszkanias
                             select new
                             {
                                 mieszkanie.Id_mieszkania,
                                 mieszkanie.Nazwa,
                                 mieszkanie.Ilosc_mieszkancow,
                                 mieszkanie.Opis
                             }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Mieszkania = query
                });
            }//using
        }//Get()

        /// <summary>
        /// Dodawanie nowego mieszkania
        /// </summary>
        /// 
        /// <param name="flatModel"> Mieszkanie w formacie JSON:
        /// {
        ///     "name":"nazwa",
        ///     "description":"opis",
        ///     "password":"haslo"
        /// }
        /// </param>
        /// 
        /// <returns>Odpowiedź HTTP z ID utworzonego mieszkania</returns>
        [HttpPost]
        public HttpResponseMessage Post(FlatModel flatModel)
        {
            int flatId;
            using (var db = new DbMieszkanieStudenckie())
            {
                var flats = db.Set<Mieszkania>();
                // zakładam, że zadziała wyzwalacz w bazie danych i on sam doda odpowiednią ilość mieszkańców
                Mieszkania flat = new Mieszkania { Nazwa = flatModel.name, Opis = flatModel.description, haslo = flatModel.password, Ilosc_mieszkancow = 0 };
                flats.Add(flat);
                db.SaveChanges(); // Po wykonaniu SaveChanges pola w obiekcie flat są aktualizowane

                flatId = flat.Id_mieszkania;
            }//using

            using (var db = new DbMieszkanieStudenckie())
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                var updatedUser = db.Uzytkownicies.First(u => u.Id_uzytkownika == userId);
                updatedUser.Id_mieszkania = flatId;
                db.Uzytkownicies.Attach(updatedUser);
                var entry = db.Entry(updatedUser);
                entry.Property(e => e.Id_mieszkania).IsModified = true;
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, flatId);
            }
            //return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL);
        }//Post()

        /// <summary>
        /// Aktualizacja nazwy i opisu mieszkania
        /// </summary>
        /// <param name="flatModel">JSON z id, nową nazwą i opisem mieszkania</param>
        /// <returns>Zaktualizowany obiekt mieszkania</returns>
        [HttpPut]
        public HttpResponseMessage Put(FlatModel flatModel)
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                if (AuthorizationHelper.isUserAuthorized(Request.Headers, flatModel.id))
                {
                    // aktualizacja krotki
                    var updatedFlat = new Mieszkania { Id_mieszkania = flatModel.id, Nazwa = flatModel.name, Opis = flatModel.description };
                    db.Mieszkanias.Attach(updatedFlat);
                    var entry = db.Entry(updatedFlat);
                    entry.Property(e => e.Nazwa).IsModified = true;
                    entry.Property(e => e.Opis).IsModified = true;
                    db.SaveChanges();
                }//if
                else
                {
                    // odmowa dostępu
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);
                }//else
            }//using

            // w tym samym bloku using, w którym aktualizowałem krotkę, 
            // nie mogłem pobrać prawidłowej wartości pola Ilosc_mieszkancow
            using (var db = new DbMieszkanieStudenckie())
            {
                Mieszkania flat = db.Mieszkanias.First(f => f.Id_mieszkania == flatModel.id);
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    flat.Id_mieszkania,
                    flat.Nazwa,
                    flat.Ilosc_mieszkancow
                });
            }//using            
        }//Put()

        /// <summary>
        /// Usuwanie mieszkania
        /// </summary>
        /// <param name="flatModel">ID mieszkania do usunięcia w formacie JSON</param>
        /// <returns>Success jeśli usunięto mieszkanie lub informację o błędzie</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(FlatModel flatModel)
        {
            // autoryzacja
            if(!AuthorizationHelper.isUserAuthorized(Request.Headers, flatModel.id))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);

            using (var db = new DbMieszkanieStudenckie())
            {
                // znalezienie mieszkania do usunięcia w bazie
                var flat = db.Mieszkanias.First(f => f.Id_mieszkania == flatModel.id);
                if(flat != null)
                {
                    try
                    {
                        // usuwanie zależności z całej bazy danych
                        deleteFlatDependencies(flatModel);

                        // usuwanie mieszkania
                        db.Mieszkanias.Remove(flat);
                        db.SaveChanges();
                    }//try
                    catch (Exception ex)
                    {
                        Console.Out.WriteLine(ex);
                        return Request.CreateErrorResponse(HttpStatusCode.OK, ex);
                    }//catch
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//if
                else
                {
                    // nie znaleziono takiego mieszkania
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.NOT_FOUND + " mieszkania.");
                }
            }//using
        }//Delete()

        /// <summary>
        /// Usuwanie danych związanych z mieszkaniem ze wszystkich tabel w bazie z wyjątkiem tabeli Mieszkania
        /// </summary>
        /// <param name="flatModel">Mieszkanie, którego dane mają zostać usunięte</param>
        private void deleteFlatDependencies(FlatModel flatModel)
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                // usuwanie użytkowników z mieszkania
                var usersList = (from user in db.Uzytkownicies
                                 where user.Id_mieszkania == flatModel.id
                                 select user).ToList();
                foreach (var user in usersList)
                {
                    var updatedUser = user;
                    updatedUser.Id_mieszkania = null;
                    db.Uzytkownicies.Attach(updatedUser);
                    var entry = db.Entry(updatedUser);
                    entry.Property(e => e.Id_mieszkania).IsModified = true;
                }//foreach

                // usuwanie budżetu mieszkania
                var budgetList = (from budget in db.Budzets
                                  where budget.Id_mieszkania == flatModel.id
                                  select budget).ToList();
                foreach (var budget in budgetList)
                {
                    db.Budzets.Remove(budget);
                }//foreach

                // usuwanie ogłoszeń
                var announcementList = (from announcement in db.Ogloszenias
                                        where announcement.Id_mieszkania == flatModel.id
                                        select announcement).ToList();
                foreach (var announcement in announcementList)
                {
                    db.Ogloszenias.Remove(announcement);
                }//foreach

                // usuwanie głosowań
                var votesList = (from vote in db.Glosowanies
                                 where vote.Id_mieszkania == flatModel.id
                                 select vote).ToList();
                foreach (var vote in votesList)
                {
                    db.Glosowanies.Remove(vote);
                }//foreach

                // usuwanie grafiku
                var schedulesList = (from schedule in db.Grafiks
                                     where schedule.Id_mieszkania == flatModel.id
                                     select schedule).ToList();
                foreach (var schedule in schedulesList)
                {
                    db.Grafiks.Remove(schedule);
                }//foreach

                // Zapisywanie zmian do bazy
                db.SaveChanges();
            }//using
        }//deleteFlatDependencies()
    }//FlatsController
}
    