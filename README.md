# README #

Aplikacja do wspomagania zarządzania mieszkaniem studenckim. Projekt realizowany na Politechnice Poznańskiej w ramach przedmiotu Projekt Zespołowy.

### Funkcjonalność systemu ###

* grupowanie użytkowników w ramach mieszkań
* ułatwienia zarządzania budżetem
* system ogłoszeń

### Uruchomienie projektu ###

Do uruchomienia projektu wymagane są:

* Microsoft Visual Studio 2015 (z narzędziami dla ASP.NET i Xamarin)
* Microsoft SQL Server 2014 Management Studio

[Instalacja i urchomienie bazy danych oraz serwera REST](https://bitbucket.org/programiscids6/pz-mieszkanie-studenckie/src/ce36e5e489946013a90548e683bacc22854f5032/MieszkanieStudenckieSerwer/Instrukcja%20instalacji%20serwera.txt?at=master&fileviewer=file-view-default)

Po zakończeniu konfiguracji serwera można uruchomić aplikację mobilną. W tym celu należy otworzyć solucję **Xamarin Android**, skompilować program i zainstalować na maszynie wirtualnej lub fizycznym urządzeniu z systemem Android.