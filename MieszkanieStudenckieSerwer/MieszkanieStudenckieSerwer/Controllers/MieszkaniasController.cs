﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MieszkanieStudenckieSerwer.DatabaseClasses;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Kontroler testowy - nie używać, zostanie usunięty.
    /// </summary>
    public class MieszkaniasController : ApiController
    {
        private DbMieszkanieStudenckie db = new DbMieszkanieStudenckie();

        // GET: api/Mieszkanias
        public IQueryable<Mieszkania> GetMieszkanias()
        {
            return db.Mieszkanias;
        }

        // GET: api/Mieszkanias/5
        [ResponseType(typeof(Mieszkania))]
        public async Task<IHttpActionResult> GetMieszkania(int id)
        {
            Mieszkania mieszkania = await db.Mieszkanias.FindAsync(id);
            if (mieszkania == null)
            {
                return NotFound();
            }

            return Ok(mieszkania);
        }

        // PUT: api/Mieszkanias/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMieszkania(int id, Mieszkania mieszkania)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mieszkania.Id_mieszkania)
            {
                return BadRequest();
            }

            db.Entry(mieszkania).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MieszkaniaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Mieszkanias
        [ResponseType(typeof(Mieszkania))]
        public async Task<IHttpActionResult> PostMieszkania(Mieszkania mieszkania)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Mieszkanias.Add(mieszkania);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = mieszkania.Id_mieszkania }, mieszkania);
        }

        // DELETE: api/Mieszkanias/5
        [ResponseType(typeof(Mieszkania))]
        public async Task<IHttpActionResult> DeleteMieszkania(int id)
        {
            Mieszkania mieszkania = await db.Mieszkanias.FindAsync(id);
            if (mieszkania == null)
            {
                return NotFound();
            }

            db.Mieszkanias.Remove(mieszkania);
            await db.SaveChangesAsync();

            return Ok(mieszkania);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MieszkaniaExists(int id)
        {
            return db.Mieszkanias.Count(e => e.Id_mieszkania == id) > 0;
        }
    }
}