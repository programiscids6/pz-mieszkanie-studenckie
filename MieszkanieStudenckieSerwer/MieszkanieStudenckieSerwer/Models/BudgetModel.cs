﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace MieszkanieStudenckieSerwer.Models
{
    public class BudgetModel
    {
        public int budgetId { get; set; }
        public int flatId { get; set; }
        public int userId { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public string photo { get; set; }
    }
}