# README #

Serwer. Dokument podzielony jest na dwie części:
 - aplikacja mobilna (opis pod kątem łączenia się z serwerem - czyli użycie ConnectionClass)
 - serwer

*****************************
***** Aplikacja mobilna *****
*****************************


Do łączenia się z serwerem służy klasa ConnectionClass. Metody:
 - login(string nazwa, string haslo) - logowanie
 - logout
 - sendData(string url_zasobu_na_serwerze, string dane) - przesłanie danych na serwer

Dokładniejszy opis znajduje się w kodzie klasy - każda metoda jest opatrzona odpowiednim komentarzem.

******************
***** Serwer *****
******************

### Podstawowe api: ###

* api/test - testowy kontroler
* api/account - obsługa kont użytkowników (logowanie, rejestracja)
* api/flats - obsługa mieszkań (lista, dodawanie nowych, aktualizacja)
* api/myflat - kontroler moje mieszkanie
* api/budget - zarządzanie budżetem
* api/publication - ogłoszenia
* api/vote - głosowania
* api/imagetest - testowanie przesyłania obrazu

### api/test: ###

* GET - zwraca listę użytkowników w formacie JSON:
	{
		"Uzytkownicy": [
		{
			"Id_uzytkownika":id(int),
			"Id_mieszkania":id(int),
			"login":login(string),
			"haslo":haslo(string)
		},
		{
			"Id_uzytkownika":id(int),
			"Id_mieszkania":id(int),
			"login":"login",
			"haslo":"haslo"
		}
		]
	}

### api/account: ###

* GET - logowanie, zwraca: "Success" w przypadku zalogowania
* POST: api/account/register - dodawanie nowego użytkownika, przyjmuje JSON:
	{
		"username":"login",
		"password":"haslo"
	}
* PUT: api/account/addToFlat - dołączenie do mieszkania, przyjmuje:
	{
        "id":id(int),
		"password":"haslo_mieszkania"
    }

### api/flats: ###

* GET - lista mieszkań (JSON)
* POST - dodanie nowego mieszkania, przyjmuje JSON:
	{
        "name":"nazwa",
		"description":"opis",
		"password":"haslo"
	}
* PUT - aktualizacja nazwy i opisu mieszkania, przyjmuje JSON:
	{
        "id":id(int),
        "name":"nazwa",
		"description":"opis"
	}
	
	
### api/myflat ###

* GET - mieszkanie użytkownika
* PUT - aktualizacja danych mieszkania, przyjmuje:
	{
		"id": id(int),
		"name": "nazwa",
		"description": "opis",
		"password":"haslo"
	}
* GET api/myflat/members - lista współlokatorów i dług pieniędzy wobec nich 
(dodatni dług oznacza, że dany użytkownik wisi nam pieniądze, a ujemny, że to my wisimy)
{
  "Uzytkownicy": [
    {
      "Login": "Jan",
      "dlug": 50
    }
  ]
}


### api/budget ###

* GET - lista rachunków z mieszkania danego użytkownika, np.
	{
		"Budgets": [
		{
			"budgetId": 1,
			"userId": 1,
			"username": "Jan",
			"name": "Woda",
			"price": 200,
			"photo": ""
		},
		{
			"budgetId": 2,
			"userId": 2,
			"username": "Stefan",
			"name": "Gaz",
			"price": 300,
			"photo": ""
		}
	   ]
	}
* POST - dodawanie nowego rachunku, przyjmuje:
	{
		"flatId":idMieszkania<int>,
		"name":nazwaRachunku<string>,
		"price":cena<double>,
		"photo":zdjęcie<string>
	}
	
* DELETE - usuwanie rachunku, przyjmuje;
	{
		"budgetId": idRachunku<int>
	}
zwraca: Success lub błąd.
	

### api/publication ###

* GET - lista ogłoszeń z mieszkania użytkownika
	{
		"Publications": [
		{
			"publicationId": 1,
			"flatId": 1,
			"userName": "Jan",
			"title": "Impreza",
			"description": "Dzisiaj impreza o 20:00",
			"beginDate": "2016-04-25T00:00:00",
		},
		{
			"publicationId": 2,
			"flatId": 1,
			"userName": "Stefan",
			"title": "Naprawa kranu",
			"description": "Jutro przychodzi hydraulik wymienić kran",
			"beginDate": "2016-04-25T00:00:00",
		}
		]
	}
	
* POST - dodawanie nowego ogłoszenia, przyjmuje:
	{
	  "title": tytuł<string>,
      "description": opis<string>
	}
  Zwraca: ID dodanego ogłoszenia lub komunikat o błędzie
  
* PUT - aktualizacja ogłoszenia, przyjmuje:
	{
	  "publicationId": idOgłoszenia<int>,
	  "title": tytuł<string>,
      "description": opis<string>
	}
  Zwraca: Success lub błąd

* DELETE - usuwanie ogłoszenia, przyjmuje:
	{
	  "publicationId": idOgłoszenia
	}
  Zwraca: Success lub błąd
  
  
### api/vote ###

* GET - zwraca listę głosowań w formacie:
{
	"Votes": [
		{
			"voteId": idGłosowania<int>,
			"flatId": idMieszkania<int>,
			"name": nazwaGłosowania<string>,
			"description": opisGłosowania<string>,
			"voteYes": liczbaGłosówZa<int>,
			"voteNo": liczbaGłosówPrzeciw<int>,
			"beginDate": dataRozpoczęciaGłosowania<DateTime>,
			"endDate": dataZakończeniaGłosowania<DateTime>,
			"avaliable": czyUżytkownikMożeOddaćGłos<bool>,
			"myVote": głosUżytkownika<bool?>
		}
	]
}

myVote jest nullable (bool?). Jeśli użytkownik nie głosował w danym głosowaniu to przyjmuje
wartość null, jeśli głosował na tak to ture i false w przeciwnym wypadku.

* POST - dodawanie nowego głosowania, przyjmuje: 
		{
			"name": nazwaGłosowania<string>,
			"description": opisGłosowania<string>,
			"endDate": dataZakończeniaGłosowania<DateTime>
		}
  zwraca: Success lub komunikat o błędzie.
  
* PUT - oddanie głosu, przyjmuje:
	{
		"voteId": idGłosowania<int>,
		"userVote": głosUżytkownika<bool>
	}
	gdzie userVote==true oznacza głos na tak 
	
	
### api/imagetest ###

* GET - zwraca obraz jako base64 string
* POST - dodawanie obrazu, przyjmuje:
	{
		"image":obraz<base64string>,
		"description":opisObrazu<string>
	}