﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MieszkanieStudenckieSerwer.Models;
using System;
namespace MieszkanieStudenckieSerwer.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class BudgetController : ApiController
    {
        /// <summary>
        /// GET: api/budget
        /// 
        /// Zwraca wszytkie informacje o zakupach z mieszkania aktualnie zalogowanego użytkownika
        /// </summary>
        /// <returns>Lista rachunków</returns>
        public HttpResponseMessage Get()
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                int? id = AuthorizationHelper.getUserFlatId(AuthorizationHelper.getUserId(Request.Headers));
                var query = (from Budzet in db.Budzets
                             join users in db.Uzytkownicies on Budzet.Id_uzytkownika equals users.Id_uzytkownika
                             where id == Budzet.Id_mieszkania
                             select new
                             {
                                 budgetId = Budzet.Id_budzetu,
                                 userId = Budzet.Id_uzytkownika,
                                 username = users.Login,
                                 name = Budzet.Nazwa,
                                 price = Budzet.Cena,
                                 photo = Budzet.Zdjecie
                             }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Budgets = query
                });
            }//using
        }//Get()

        /// <summary>
        /// POST: api/budget
        /// 
        /// Dodawanie nowego rachunku do budżetu
        /// </summary>
        /// <param name="budget">Dane rachunku (patrz BudgetModel)</param>
        /// <returns>ID dodanego rachunku lub komunikat o błędzie</returns>
        public HttpResponseMessage Post(BudgetModel budget)
        {
            int budgetId;
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            try
            {
                using (var db = new DbMieszkanieStudenckie())
                {
                    var budgets = db.Set<Budzet>();
                    Budzet newBudget = new Budzet { Id_mieszkania = budget.flatId, Id_uzytkownika = userId, Cena = budget.price, Nazwa = budget.name, Zdjecie = budget.photo };
                    budgets.Add(newBudget);
                    db.SaveChanges(); // Po wykonaniu SaveChanges pola w obiekcie są aktualizowane

                    budgetId = newBudget.Id_budzetu;

                    return Request.CreateResponse(HttpStatusCode.OK, budgetId);
                }//using
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Post()

        /// <summary>
        /// Usuwanie rachunku
        /// </summary>
        /// <param name="budget">ID rachunku do usunięcia</param>
        /// <returns>Success lub błąd</returns>
        public HttpResponseMessage Delete(BudgetModel budget)
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            int? flatId = AuthorizationHelper.getUserFlatId(Request.Headers);

            try
            {
                using (var db = new DbMieszkanieStudenckie())
                {
                    var toDel = db.Budzets.First(d => d.Id_budzetu==budget.budgetId && d.Id_uzytkownika==userId);
                    db.Budzets.Remove(toDel);
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }
            }//try
            catch(InvalidOperationException)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Debts()
    }
}
