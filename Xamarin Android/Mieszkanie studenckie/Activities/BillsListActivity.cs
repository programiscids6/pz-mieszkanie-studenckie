using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "BillsListActivity")]
    public class BillsListActivity : ListActivity
    {
        List<Model.BillModel> billsList;
        List<string> billsname;
        JsonValue billsJson;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            billsList = new List<Model.BillModel>();
            billsname = new List<string>();
            getListViewData();

            // Create your application here
        }
        private async void getListViewData()
        {
            try
            {
                var progressDialog = ProgressDialog.Show(this, "Prosz� czeka�", "Trwa wczytywanie...", true);
                JsonValue json = await ConnectionClass.getDataAsJson("api/budget");
                ParseToModel(json);
                progressDialog.Hide();
                if (billsname.Count != 0)
                    ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, billsname.ToArray());
                else
                    Toast.MakeText(this, "Brak rachunk�w", ToastLength.Long).Show();
            }
            catch
            {
                Toast.MakeText(this, "Brak rachunk�w do wy�wietlenia", ToastLength.Long).Show();
                base.OnBackPressed();
            }        
             
        }
        private void ParseToModel(JsonValue json)
        {
            billsJson = json["Budgets"];

            for (int i = 0; i < billsJson.Count; i++)
            {
                JsonValue item = billsJson[i];
                Model.BillModel bill = new Model.BillModel();
                bill.budgetId = item["budgetId"];
                bill.userId = item["userId"];
                bill.userName = item["username"];
                bill.billName = item["name"];
                bill.price = item["price"];
                bill.photo = item["photo"];
                billsname.Add(bill.billName);
                billsList.Add(bill);
            }
        }
        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            try
            {
                var intent = new Intent(this, typeof(BillDetailsActivity));
                string[] userData = new string[] { billsList[position].budgetId.ToString(), billsList[position].userId.ToString(), billsList[position].userName,
                    billsList[position].billName.ToString(), billsList[position].price.ToString(),billsList[position].photo.ToString()};
                // intent.PutExtra("userJson", usersJson[position].ToString());
                intent.PutExtra("JsonData", userData);
                StartActivity(intent);
            }
            catch
            {
                RunOnUiThread(() =>
                {
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(this);
                    builder.SetTitle("B��d podczas pobierania danych");
                    builder.SetMessage("Prosz� spr�bowa� ponownie.");
                    builder.SetCancelable(false);
                    builder.SetPositiveButton("OK", delegate { base.OnBackPressed(); });
                    builder.Show();
                }
                    );
                
            }        
        }
    }
}