use [Mieszkanie studenckie]

-- zmiana kodowania na obs�uguj�ce j�zyk polski 
ALTER DATABASE [Mieszkanie studenckie] COLLATE Polish_CI_AS
GO

-- ---------------
-- tworzenie tabel
-- ---------------

Create Table Mieszkania(
Id_mieszkania int Primary Key Identity(1,1),
Nazwa nvarchar(30),
Ilosc_mieszkancow int);

Create Table Uzytkownicy(
Id_uzytkownika int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
login nvarchar(30),
haslo  nvarchar(30));

Create Table Budzet(
Id_budzetu int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Nazwa nvarchar(30),
Cena float);

Create Table Ogloszenia(
Id_ogloszenia int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Tresc nvarchar(250));

Create Table Grafik(
Id_grafiku int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Od_kiedy date,
Do_kiedy date,
Tresc nvarchar(100));

Create Table Glosowanie(
Id_glosowania int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Nazwa nvarchar(30),
Glosy_za int,
Glosy_przeciw int);


-- -----------------------
-- Wypa�nianie bazy danymi
-- -----------------------

-- tabela Mieszkania
INSERT INTO Mieszkania
VALUES ('Mieszkanie Jana', 2);

-- tabela Uzytkownicy
INSERT INTO Uzytkownicy
VALUES (1, 'Jan', 'haslo');
INSERT INTO Uzytkownicy
VALUES (1, 'Stefan', 'haslo');
INSERT INTO Uzytkownicy
VALUES (null, 'Marek', 'haslo');

-- tabela Ogloszenia
INSERT INTO Ogloszenia
VALUES (1, 1, 'Dzisiaj impreza o 20:00');
INSERT INTO Ogloszenia
VALUES (1, 2, 'Jutro przychodzi hydraulik wymieni� kran');

-- tabela Budzet
INSERT INTO Budzet
VALUES (1, 1, 'Woda', 200);
INSERT INTO Budzet
VALUES (1, 2, 'Gaz', 300);

-- tabela Glosowanie
INSERT INTO Glosowanie
VALUES (1, 1, 'Impreza za tydzie�?', 0, 0);

-- tabela Grafik
INSERT INTO Grafik
VALUES (1, 1, '2016-04-10', '2016-04-17', 'Sprz�tanie �azienki');

-- odpytanie bazy o ka�d� tabel�
SELECT * FROM Budzet;
SELECT * FROM Glosowanie;
SELECT * FROM Grafik;
SELECT * FROM Mieszkania;
SELECT * FROM Ogloszenia;
SELECT * FROM Uzytkownicy;