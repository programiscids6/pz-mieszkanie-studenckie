﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using MieszkanieStudenckieSerwer.Filters;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Kontroler testowy
    /// </summary>
    public class testController : ApiController
    {
        /// <summary>
        /// HTTP GET api/test
        /// </summary>
        /// <returns>tabela Uzytkownicy z bazy danych typu String</returns>
        public HttpResponseMessage Get()
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                var query = (from users in db.Uzytkownicies
                             select new
                            {
                                users.Id_uzytkownika,
                                users.Id_mieszkania,
                                users.Login,
                                users.Haslo
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Uzytkownicy = query
                });
            }// using
        }//Get()

        [Route("api/test/post")]
        [HttpPost]
        public HttpResponseMessage Post(Dane dane)
        {
            return Request.CreateResponse(HttpStatusCode.OK, dane);
        }//Post()
    }//testController

    /// <summary>
    /// Klasa pomocnicza
    /// </summary>
    public class Dane
    {
        public string username;
        public string password;
    }
}
