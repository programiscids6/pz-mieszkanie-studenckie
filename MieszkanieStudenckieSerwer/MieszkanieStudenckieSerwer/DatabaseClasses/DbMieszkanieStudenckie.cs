namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DbMieszkanieStudenckie : DbContext
    {
        public DbMieszkanieStudenckie()
            : base("name=DbMieszkanieStudenckie")
        {
        }

        public virtual DbSet<Budzet> Budzets { get; set; }
        public virtual DbSet<Glosowanie> Glosowanies { get; set; }
        public virtual DbSet<Grafik> Grafiks { get; set; }
        public virtual DbSet<Kto_Glosowal> Kto_Glosowal { get; set; }
        public virtual DbSet<Mieszkania> Mieszkanias { get; set; }
        public virtual DbSet<Ogloszenia> Ogloszenias { get; set; }
        public virtual DbSet<Uzytkownicy> Uzytkownicies { get; set; }
        public virtual DbSet<database_firewall_rules> database_firewall_rules { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<database_firewall_rules>()
                .Property(e => e.start_ip_address)
                .IsUnicode(false);

            modelBuilder.Entity<database_firewall_rules>()
                .Property(e => e.end_ip_address)
                .IsUnicode(false);
        }
    }
}
