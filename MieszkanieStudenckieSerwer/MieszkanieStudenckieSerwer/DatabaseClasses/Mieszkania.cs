namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Mieszkania")]
    public partial class Mieszkania
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Mieszkania()
        {
            Budzets = new HashSet<Budzet>();
            Glosowanies = new HashSet<Glosowanie>();
            Grafiks = new HashSet<Grafik>();
            Ogloszenias = new HashSet<Ogloszenia>();
            Uzytkownicies = new HashSet<Uzytkownicy>();
        }

        [Key]
        public int Id_mieszkania { get; set; }

        [StringLength(30)]
        public string Nazwa { get; set; }

        public string Opis { get; set; }

        public string haslo { get; set; }

        public int? Ilosc_mieszkancow { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Budzet> Budzets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Glosowanie> Glosowanies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Grafik> Grafiks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ogloszenia> Ogloszenias { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Uzytkownicy> Uzytkownicies { get; set; }
    }
}
