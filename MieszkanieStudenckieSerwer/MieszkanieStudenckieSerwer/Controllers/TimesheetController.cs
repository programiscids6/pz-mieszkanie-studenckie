﻿using MieszkanieStudenckieSerwer.DatabaseClasses;
using MieszkanieStudenckieSerwer.Filters;
using MieszkanieStudenckieSerwer.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MieszkanieStudenckieSerwer.Models;
using System;
namespace MieszkanieStudenckieSerwer.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class TimesheetController : ApiController
    {
        public HttpResponseMessage Get()
            // Zwraca wszytkie grafiki z mieszkania aktualnie zalogowaneg uzytkownika
        {
            using (var db = new DbMieszkanieStudenckie())
            {
                int? id =  AuthorizationHelper.getUserFlatId( AuthorizationHelper.getUserId(Request.Headers));
                var query = (from grafik in db.Grafiks
                             where id == grafik.Id_mieszkania
                             select new
                             {
                                 grafik.Id_grafiku,
                                 grafik.Id_mieszkania,
                                 grafik.Id_uzytkownika,
                                 grafik.Od_kiedy,
                                 grafik.Do_kiedy,
                                 grafik.Tresc                            
            }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                Grafik = query
            });

             }
          }


        [HttpPost]
        public HttpResponseMessage Post(TimesheetModel timesheetmodel)
        {
            int? timesheetId;
            int userid = AuthorizationHelper.getUserId(Request.Headers);
            int? flatid = AuthorizationHelper.getUserFlatId(userid);
            using (var db = new DbMieszkanieStudenckie())
            {
                
                var timesheets = db.Set<Grafik>();
                // zakładam, że zadziała wyzwalacz w bazie danych i on sam doda odpowiednią ilość mieszkańców
                Grafik timesheet = new Grafik { Id_uzytkownika = userid, Id_mieszkania = flatid, Od_kiedy = timesheetmodel.od_kiedy, Do_kiedy = timesheetmodel.do_kiedy, Tresc = timesheetmodel.tresc };
                timesheets.Add(timesheet);
                db.SaveChanges(); // Po wykonaniu SaveChanges pola w obiekcie flat są aktualizowane

                timesheetId = timesheet.Id_mieszkania;
            }//using

            return Request.CreateResponse(HttpStatusCode.OK, timesheetId);
            //return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL);
        }//Post()

    }
}
