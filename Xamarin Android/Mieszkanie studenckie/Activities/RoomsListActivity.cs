using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "RoomsListActivity")]
    public class RoomsListActivity : ListActivity
    {        
        List<Model.UserModel> usersList;
        List<string> usersLogins;       
        JsonValue usersJson;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            usersList = new List<Model.UserModel>();
            usersLogins = new List<string>();  
            getListViewData();           
            
        }
        
        private async void getListViewData()
        {
           // string url = "http://pzserwer.azurewebsites.net/api/test";

            // JsonValue json = await FetchDataAsync(url);
            JsonValue json = await ConnectionClass.getDataAsJson("api/flats");

            ParseToModel(json);
            if(usersLogins.Count!=0)
                ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, usersLogins.ToArray());
            else
                Toast.MakeText(this, "Brak pokoj�w w systemie", ToastLength.Long).Show();
        }

        private void ParseToModel(JsonValue json)
        {
            usersJson = json["Mieszkania"];
                       
            for (int i=0;i< usersJson.Count;i++)
            {
                JsonValue item = usersJson[i];
                Model.UserModel user = new Model.UserModel();
                user.Id_uzytkownika = item["Id_mieszkania"];
                if (item["Ilosc_mieszkancow"] == null) user.Id_mieszkania = 0;
                else
                {
                  user.Id_mieszkania = item["Ilosc_mieszkancow"];
                }
                user.Login = item["Nazwa"];
                user.Opis = item["Opis"];
                usersLogins.Add(user.Login);                
                usersList.Add(user);
            }

        }

        private async Task<JsonValue> FetchDataAsync(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    // Return the JSON document:
                    return jsonDoc;
                }
            }
        }

        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {            
            var intent = new Intent(this, typeof(RoomDetailsActivity));
            string[] userData = new string[] { usersList[position].Login.ToString(), usersList[position].Id_mieszkania.ToString(), usersList[position].Opis, usersList[position].Id_uzytkownika.ToString()};
            // intent.PutExtra("userJson", usersJson[position].ToString());
            intent.PutExtra("JsonData", userData);            
            StartActivity(intent);  
        }
    }
}