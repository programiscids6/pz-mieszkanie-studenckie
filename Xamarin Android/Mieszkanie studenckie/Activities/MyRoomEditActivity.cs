using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "EditRoomActivity")]
    public class MyRoomEditActivity : Activity
    {
        string roomId;
        string roomName;
        string roomDescription;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MyRoomEditLayout);

            EditText roomNameText = FindViewById<EditText>(Resource.Id.editRoomNameText);
            EditText roomDescriptionText = FindViewById<EditText>(Resource.Id.editRoomDescriptionText);
            Button editRoomButton = FindViewById<Button>(Resource.Id.editRoomButton);

            string[] RoomDetails = Intent.GetStringArrayExtra("JsonData");
            roomNameText.Text = RoomDetails[0];
            roomDescriptionText.Text = RoomDetails[1];
            roomId = RoomDetails[2];

            roomName = RoomDetails[0];
            roomDescription = RoomDetails[1];

            //  roomName = " {\"name\":\" "+ addRoomText.Text+ " \"} ";
            // SendData("api/flats",roomName);

            editRoomButton.Click += (sender, e) =>
            {
                if (String.IsNullOrEmpty(roomNameText.Text))
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Nazwa mieszkania nie mo�e by� pusta.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { });
                        builder.Show();
                    }
                  );
                }                
                else if (String.IsNullOrEmpty(roomDescriptionText.Text))
                {
                    RunOnUiThread(() =>
                    {
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(this);
                        builder.SetTitle("B��d");
                        builder.SetMessage("Opis nie mo�e by� pusty.");
                        builder.SetCancelable(false);
                        builder.SetPositiveButton("OK", delegate { });
                        builder.Show();
                    }
                  );
                }
                else
                {
                    string roomJSON = @" {""id"":" + roomId + @", ""name"":""" + roomNameText.Text + @""", ""description"":""" + roomDescriptionText.Text + @""" } ";
                    //roomName = @" "" } " +roomName ; 
                    UpdateData("api/myflat", roomJSON);
                    Toast.MakeText(this, "Zmieniono dane pokoju", ToastLength.Long).Show();
                    base.OnBackPressed();
                    
                    
                }
            };

            // Create your application here
        }

       

        private async void UpdateData(string url, string data)
        {
            await Connection.ConnectionClass.sendData(url, data,"PUT");
        }
    }
}