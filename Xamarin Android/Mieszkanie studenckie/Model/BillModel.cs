using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Model
{
    class BillModel
    {
        public int budgetId { get; set; }
        public int userId { get; set; }
        public string userName { get; set; }
        public string billName { get; set; }
        public double price { get; set; }    
        public string photo { get; set; }
    }
}