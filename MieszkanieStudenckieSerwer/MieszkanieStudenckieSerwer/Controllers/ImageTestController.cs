﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MieszkanieStudenckieSerwer.Controllers
{
    public class ImageTestController : ApiController
    {
        static string myImg = "";
        static string des = "";

        public class ImgModel
        {
            public string image;
            public string description;
        }

        // GET: api/ImageTest
        public string Get()
        {
            return myImg;
        }

        
        // POST: api/ImageTest
        public void Post([FromBody]ImgModel value)
        {
            myImg = value.image;
            des = value.description;
        }
    }
}
