using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Provider;
using Java.IO;
using Android.Graphics;
using Uri = Android.Net.Uri;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "AddBillActivity")]
    public class AddBillActivity : Activity
    {
        public static File _file;
        public static File _dir;
        public static Bitmap bitmap;
        public bool czyZrobionoZdjecie = false;
        ImageView imageView;
        public string roomId;
        // public List<Model.ProductItemModel> productList;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddBillLayout);

           // productList= new List<Model.ProductItemModel>();
            Button addBill = FindViewById<Button>(Resource.Id.AddBillPhotoButton);
            Button saveBill = FindViewById<Button>(Resource.Id.AddBillSaveButton);
            EditText billNameText = FindViewById<EditText>(Resource.Id.AddBillNameText);
            EditText billPriceText = FindViewById<EditText>(Resource.Id.AddBillAmountText);
            imageView = FindViewById<ImageView>(Resource.Id.AddBillImage);

            string[] RoomDetails = Intent.GetStringArrayExtra("JsonData");            
            roomId = RoomDetails[0];

            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();

                addBill.Click += TakeAPicture;
            }
            else
            {
                Toast.MakeText(this, "B��d aplikacji Aparat", ToastLength.Long).Show();
            }

            saveBill.Click += (sender, e) =>
            {

                //Bitmap bm = BitmapFactory.DecodeFile("/path/to/image.jpg");
                //
                //ByteArrayOutputStream baos = new ByteArrayOutputStream();
                //System.IO.MemoryStream stream = new System.IO.MemoryStream();
                //System.IO.Stream stream = new System.IO.Stream();
                ////System.IO.Stream stream = baos;
                //
                ////bm.Compress(Bitmap.CompressFormat.Jpeg, 100, baos); //bm is the bitmap object 
                //bm.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                //byte[] b = baos.ToByteArray();

                if (czyZrobionoZdjecie)
                {
                    //int height = Resources.DisplayMetrics.HeightPixels;
                    //int width = imageView.Height;
                    //Bitmap bm = _file.Path.LoadAndResizeBitmap(width, height);

                    int height = Resources.DisplayMetrics.HeightPixels;
                    int width = imageView.Height;
                    Bitmap bm = _file.Path.LoadAndResizeBitmap(width, height);
                   // Bitmap bm = BitmapFactory.DecodeFile(_file.Path);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    bm.Compress(Bitmap.CompressFormat.Jpeg, 20, stream);                    
                    byte[] byteArray = stream.ToArray();
                    stream.Flush();
                    WyslijObraz(byteArray, billNameText.Text, billPriceText.Text.ToString(), roomId);
                    

                }
                else
                {
                    Toast.MakeText(this, "Zr�b zdj�cie aby wys�a� rachunek", ToastLength.Long).Show();
                }

            };

                // Create your application here
            }

        public async void WyslijObraz(byte[]  byteArray, string billName, string billPrice, string roomId)
        {
           
            // konwersja byte[] do base64string
            string base64String = Convert.ToBase64String(byteArray);           

            string data = "{ \"flatId\":" + roomId + ", \"name\":\"" + billName + "\", \"price\":" + billPrice + ", \"photo\":\"" + base64String + "\" }";
            var progressDialog = ProgressDialog.Show(this, "Prosz� czeka�", "Trwa wysy�anie...", true);
           string response= await ConnectionClass.sendData("api/budget", data);
           progressDialog.Hide();

            if (response.Length < 5)
            {
                Toast.MakeText(this, "Zapisano rachunek", ToastLength.Long).Show();
                base.OnBackPressed();
            }
            else
            {
                RunOnUiThread(() =>
                {
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(this);
                    builder.SetTitle("B��d podczas wysy�ania");
                    builder.SetMessage("Prosz� doda� rachunek ponownie.");
                    builder.SetCancelable(false);
                    builder.SetPositiveButton("OK", delegate { });
                    builder.Show();
                }
                );
            }
            //

          //  base.OnBackPressed();
        }


        private void CreateDirectoryForPictures()
        {
            _dir = new File(
                Android.OS.Environment.GetExternalStoragePublicDirectory(
                    Android.OS.Environment.DirectoryPictures), "BudzetDomowy");
            if (!_dir.Exists())
            {
                _dir.Mkdirs();
            }
        }

        private bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
                PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

        private void TakeAPicture(object sender, EventArgs eventArgs)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            _file = new File(_dir, String.Format("rachunek_{0}.jpg", Guid.NewGuid()));
            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(_file));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            // Make it available in the gallery

            Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
            Uri contentUri = Uri.FromFile(_file);
            mediaScanIntent.SetData(contentUri);
            SendBroadcast(mediaScanIntent);

            // Display in ImageView. We will resize the bitmap to fit the display.
            // Loading the full sized image will consume to much memory
            // and cause the application to crash.

            int height = Resources.DisplayMetrics.HeightPixels;
            int width = imageView.Height;
            bitmap = _file.Path.LoadAndResizeBitmap(width, height);
            if (bitmap != null)
            {
                imageView.SetImageBitmap(bitmap);
                imageView.Visibility = ViewStates.Visible;
                bitmap = null;
                czyZrobionoZdjecie=true;
            }

            // Dispose of the Java side bitmap.
            GC.Collect();
        }

        // public void addProductToList(Model.ProductItemModel product)
        // {
        //     productList.Add(product);
        // }
    }
}