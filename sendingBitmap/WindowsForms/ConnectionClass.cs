using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Json;

namespace Mieszkanie_studenckie.Connection
{
    /// <summary>
    /// Klasa statyczna odpowiedzialna za po��czenie
    /// </summary>
    static class ConnectionClass
    {
        /// <summary>
        /// Adres serwera
        /// </summary>
        public static string ServerUrl = "https://pzserwer.azurewebsites.net/";

        /// <summary>
        /// Adres do logowania na serwerze
        /// </summary>
        private static string loginUrl = "api/account";

        /// <summary>
        /// Adres rejestracji
        /// </summary>
        public static string registerUrl = ServerUrl + "api/account/register";

        private static string username = "";
        private static string pass = "";

        /// <summary>
        /// Logowanie u�ytkownika na serwerze
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="password">Has�o</param>
        /// <returns>true - zalogowano, false - b��d logowania</returns>
        public static async Task<bool> login(string login, string password)
        {
            username = login;
            pass = password;
            string status = await getDataAsString(loginUrl, true);
            if (status.Equals(Responses.SUCCESS))
            {
                return true;
            }//if

            username = "";
            pass = "";
            return false;
        }//login()

        /// <summary>
        /// Wylogowywanie u�ytkownika
        /// </summary>
        public static void logout()
        {
            username = "";
            pass = "";
        }

        /// <summary>
        /// Pobieranie danych z serwera
        /// </summary>
        /// <param name="url">Adres zasobu na serwerze (np. api/test)</param>
        /// <param name="cmd">Okre�la czy oczekiwana odpowied� to komenda serwera czy dane (domy�lne).</param>
        /// <param name="contentType">Oczekiwany typ odpowiedzi (domy�lnie tekst lub html)</param>
        /// <param name="method">Metoda uzyta do po��czenia (domy�lnie GET)</param>
        /// <returns>Dane pobrane z serwera jako �a�cuch znak�w</returns>
        public static async Task<string> getDataAsString(string url, bool cmd = false, string contentType = "text/html", string method = "GET")
        {
            url = ServerUrl + url;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = contentType;
            request.Method = method;

            if (!username.Equals(""))
            {
                // przygotowanie danch uwierzytelniaj�cych do przes�ania (zakodowanie ich)
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + pass));
                // dodanie pola autoryzacji do nag��wka HTTP
                request.Headers.Add("Authorization", "Basic " + encoded);
            }//if

            try
            {
                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                    {
                        string responseStr = stream.ReadToEnd();
                        if(cmd) 
                            return responseStr.Substring(1, responseStr.Length - 2);
                        return responseStr;
                    }//using
                }//using
            }//try
            catch (WebException ex)
            {
                var statusCode = ((HttpWebResponse)ex.Response).StatusCode;
                return "B��d: " + statusCode;
            }//catch
        }//getDataAsString()

        /// <summary>
        /// Pobieranie danych w formacie JSON
        /// </summary>
        /// <param name="url">Adres zasobu na serwerze (np. api/test)</param>
        /// <param name="contentType">Oczekiwany typ odpowiedzi (domy�lnie application/json)</param>
        /// <param name="method">Metoda uzyta do po��czenia (domy�lnie GET)</param>
        /// <returns></returns>
        public static async Task<JsonValue> getDataAsJson(string url, string contentType = "application/json", string method = "GET")
        {
            url = ServerUrl + url;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = contentType;
            request.Method = method;

            if (!username.Equals(""))
            {
                // przygotowanie danch uwierzytelniaj�cych do przes�ania (zakodowanie ich)
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + pass));
                // dodanie pola autoryzacji do nag��wka HTTP
                request.Headers.Add("Authorization", "Basic " + encoded);
            }//if

            try
            {
                using (WebResponse response = await request.GetResponseAsync())
                {
                    // Get a stream representation of the HTTP web response:
                    using (Stream stream = response.GetResponseStream())
                    {
                        // Use this stream to build a JSON document object:
                        JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                        Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                        // Return the JSON document:
                        return jsonDoc;
                    }//using
                }//using
            }//try
            catch (WebException ex)
            {
                var statusCode = ((HttpWebResponse)ex.Response).StatusCode;
                return "B��d: " + statusCode;
            }//catch
        }//getDataAsJson()

        /// <summary>
        /// Wysy�anie danych do serwera w spos�b asynchroniczny (metoda nadrz�dna)
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="data">Dane</param>
        /// <returns>Odpowied� serwera jako �a�cuch znak�w</returns>
        public static async Task<string> sendData(string url, string data)
        {
            url = ServerUrl + url;
            return await Task<string>.Factory.StartNew(() =>
            {
                return sendDataToServer(url, data);
            });
        }//sendData()

        /// <summary>
        /// W�a�ciwa metoda wys�ania danych do serwera
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="data">Dane</param>
        /// <returns>Odpowied� serwera jako �a�cuch znak�w</returns>
        private static string sendDataToServer(string url, string data)
        {
            
            var request = (HttpWebRequest)WebRequest.Create(url);

            var postData = data;
           // var dataToSend = Encoding.ASCII.GetBytes(postData);
            var dataToSend = Encoding.UTF8.GetBytes(postData);


            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = dataToSend.Length;

            if (!username.Equals(""))
            {
                // przygotowanie danch uwierzytelniaj�cych do przes�ania (zakodowanie ich)
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + pass));
                // dodanie pola autoryzacji do nag��wka HTTP
                request.Headers.Add("Authorization", "Basic " + encoded);
            }//if

            using (var stream = request.GetRequestStream())
            {
                stream.Write(dataToSend, 0, dataToSend.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            //return responseString.Substring(1, responseString.Length - 2);
            return responseString;
        }//sendDataToServer()
    }//Connection
}