using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Model
{
    class AnnouncementModel
    {
        public int publicationId { get; set; }
        public int flatId { get; set; }
        public string userName { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
    }
}