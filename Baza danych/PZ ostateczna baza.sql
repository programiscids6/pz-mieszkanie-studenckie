use [Mieszkanie studenckie]


Drop Trigger nowy_mieszkaniec
Drop Trigger usuni�cie_mieszkanca
Drop Trigger zmiana_mieszkania
Drop table Budzet
Drop table Kto_glosowal
Drop table Glosowanie
Drop table Grafik
Drop table Ogloszenia
Drop table Uzytkownicy
Drop table Mieszkania

-- zmiana kodowania na obs�uguj�ce j�zyk polski 
ALTER DATABASE [Mieszkanie studenckie] COLLATE Polish_CI_AS
GO

-- ---------------
-- tworzenie tabel
-- ---------------

Create Table Mieszkania(
Id_mieszkania int Primary Key Identity(1,1),
Nazwa nvarchar(30),
Opis nvarchar(256),
haslo nvarchar(256),
Ilosc_mieszkancow int);

Create Table Uzytkownicy(
Id_uzytkownika int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Email nvarchar(60),
Login nvarchar(30),
Haslo  nvarchar(30));

Create Table Budzet(
Id_budzetu int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Nazwa nvarchar(30),
Cena float,
Zdjecie varchar(MAX));

Create Table Ogloszenia(
Id_ogloszenia int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Tytul nvarchar(100),
Tresc nvarchar(250),
Od_kiedy date);


Create Table Grafik(
Id_grafiku int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Od_kiedy date,
Do_kiedy date,
Tresc nvarchar(100));

Create Table Glosowanie(
Id_glosowania int Primary Key Identity(1,1),
Id_mieszkania int references Mieszkania(Id_mieszkania),
Nazwa nvarchar(30),
Opis nvarchar(250),
Glosy_za int,
Glosy_przeciw int,
Od_kiedy date,
Do_kiedy date);

Create Table Kto_Glosowal(
Id_glosowania int Primary Key Identity(1,1),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Glos_za binary);
go

-- Tworzenie wyzwalaczy

go

-- Ten trigger zwi�ksza ilo�� mieszka�c�w gdy do mieszkania zostanie dodany nowy mieszkaniec
CREATE TRIGGER nowy_mieszkaniec
 ON Uzytkownicy
  AFtER insert
  as
begin 

update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow+1
From Inserted i ,Mieszkania m
where i.Id_mieszkania is not null and i.Id_mieszkania = m.Id_mieszkania

end;
go
-- Zmniejsza ilo�� mieszka�c�w po usunieciu u�ytkownika 
CREATE TRIGGER usuni�cie_mieszkanca
 ON Uzytkownicy
  AFtER delete
  as
begin 

update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow-1
From deleted d ,Mieszkania m
where d.Id_mieszkania != 0 and d.Id_mieszkania = m.Id_mieszkania

end;
go


--Zmiana mieszkania
go
CREATE TRIGGER zmiana_mieszkania
 ON Uzytkownicy
  after update
  as
begin 
update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow-1
From inserted i ,Mieszkania m, Deleted d
where  d.Id_mieszkania = m.Id_mieszkania and d.Id_mieszkania is not null

update Mieszkania  Set Ilosc_mieszkancow=Ilosc_mieszkancow+1
From inserted i ,Mieszkania m, Deleted d
where  i.Id_mieszkania = m.Id_mieszkania and i.Id_mieszkania is not null
end;
go
-- -----------------------
-- Wype�nianie bazy danymi
-- -----------------------

-- tabela Mieszkania
INSERT INTO Mieszkania
VALUES ('Mieszkanie Jana', 'Mieszkanie w kt�rym mieszka Jan i jego ekipa', 'haslo', 0);

-- tabela Uzytkownicy
INSERT INTO Uzytkownicy
VALUES (1, 'jan@wp.pl','Jan', 'haslo');
INSERT INTO Uzytkownicy
VALUES (1, 'Stefan@gmail.com','Stefan', 'haslo');
INSERT INTO Uzytkownicy
VALUES (null,'Marek@wp.pl', 'Marek', 'haslo');

-- tabela Ogloszenia
INSERT INTO Ogloszenia
VALUES (1, 1, 'Impreza','Dzisiaj impreza o 20:00','2016-04-25');
INSERT INTO Ogloszenia
VALUES (1, 2, 'Naprawa kranu','Jutro przychodzi hydraulik wymieni� kran','2016-04-25');

-- tabela Budzet
INSERT INTO Budzet
VALUES (1, 1, 'Woda', 200, '');
INSERT INTO Budzet
VALUES (1, 2, 'Gaz', 300, '');

-- tabela Glosowanie
INSERT INTO Glosowanie
VALUES (1, 1, 'Impreza za tydzie�?', 0, 0,'2016-04-25','2016-04-25');

-- tabela Grafik
INSERT INTO Grafik
VALUES (1, 1, '2016-04-10', '2016-04-17', 'Sprz�tanie �azienki');

--tabela Kto_Glosowal

-- odpytanie bazy o ka�d� tabel�
SELECT * FROM Budzet;
SELECT * FROM Glosowanie;
SELECT * FROM Grafik;
SELECT * FROM Mieszkania;
SELECT * FROM Ogloszenia;
SELECT * FROM Uzytkownicy;