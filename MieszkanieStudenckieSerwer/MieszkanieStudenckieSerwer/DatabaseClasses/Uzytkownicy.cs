namespace MieszkanieStudenckieSerwer.DatabaseClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Uzytkownicy")]
    public partial class Uzytkownicy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Uzytkownicy()
        {
            Budzets = new HashSet<Budzet>();
            Grafiks = new HashSet<Grafik>();
            Kto_Glosowal = new HashSet<Kto_Glosowal>();
            Ogloszenias = new HashSet<Ogloszenia>();
        }

        [Key]
        public int Id_uzytkownika { get; set; }

        public int? Id_mieszkania { get; set; }

        [StringLength(60)]
        public string Email { get; set; }

        [StringLength(30)]
        public string Login { get; set; }

        [StringLength(30)]
        public string Haslo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Budzet> Budzets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Grafik> Grafiks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kto_Glosowal> Kto_Glosowal { get; set; }

        public virtual Mieszkania Mieszkania { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ogloszenia> Ogloszenias { get; set; }
    }
}
