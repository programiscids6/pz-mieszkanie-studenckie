using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mieszkanie_studenckie.Connection
{
    /// <summary>
    /// Klasa statyczna zawierająca podstawowe odpowiedzi serwera
    /// </summary>
    public static class Responses
    {
        public static string SUCCESS = "Success";
    }
}