using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;
using Mieszkanie_studenckie.Connection;

namespace Mieszkanie_studenckie.Activities
{
    [Activity(Label = "MembersListActivity")]
    public class MembersListActivity : Activity
    {
        List<Model.MemberBudgetModel> membersList;
        List<string> membersLogins;
        JsonValue membersJson;
        private ListView mListView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MembersListLayout);
            mListView = FindViewById<ListView>(Resource.Id.MembersListView);
            membersList = new List<Model.MemberBudgetModel>();
            membersLogins = new List<string>();
            getListViewData();

            // Create your application here
        }
        private async void getListViewData()
        {
            // string url = "http://pzserwer.azurewebsites.net/api/test";

            // JsonValue json = await FetchDataAsync(url);
            try
            {
                JsonValue json = await ConnectionClass.getDataAsJson("api/myflat/members");

                ParseToModel(json);
                if (membersLogins.Count != 0)
                {

                    // ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, membersLogins.ToArray());
                    MyListViewAdapter adapter = new MyListViewAdapter(this, membersList);                    
                    mListView.Adapter = adapter;
                }
                else
                    Toast.MakeText(this, "Brak wsp�lokator�w", ToastLength.Long).Show();
            }
            catch
            {
                Toast.MakeText(this, "B��d przy wczytywaniu wsp�lokator�w", ToastLength.Long).Show();
                base.OnBackPressed();
            }

        }

        private void ParseToModel(JsonValue json)
        {
            membersJson = json["Uzytkownicy"];

            for (int i = 0; i < membersJson.Count; i++)
            {
                JsonValue item = membersJson[i];
                Model.MemberBudgetModel member = new Model.MemberBudgetModel();
                member.login = item["Login"];
                
                member.dlug = item["dlug"];                
                membersLogins.Add(member.login);
                membersList.Add(member);
            }

        }
    }
}