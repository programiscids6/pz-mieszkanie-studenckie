﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MieszkanieStudenckieSerwer.Models
{
    /// <summary>
    /// Klasa pomocnicza reprezentująca dane potrzebne do utworzenia mieszkania
    /// </summary>
    public class FlatModel
    {
        public int id;
        public string name;
        public string description;
        public string password;
    }//FlatModel
}